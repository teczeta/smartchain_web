<?php

namespace App\Filament\Resources\InsightResource\Pages;

use App\Filament\Resources\InsightResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateInsight extends CreateRecord
{
    protected static string $resource = InsightResource::class;
}
