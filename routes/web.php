<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::middleware('cache.headers:public;max_age=2628000')->group(function () {

    Route::view('/', 'pages.home')->name('home');

    Route::view('/business-consulting-service', 'pages.business-advisory-service')->name('services.business-advisory-service');
    Route::view('/implementation-service', 'pages.implementation-service')->name('services.implementation-service');
    Route::view('/technology-consulting-service', 'pages.technology-advisory-service')->name('services.technology-advisory-service');

    Route::view('/supply-chain-planning', 'pages.supply-chain-planning')->name('solution.supply-chain-planning');
    Route::view('/supply-chain-execution', 'pages.supply-chain-execution')->name('solution.supply-chain-execution');

    Route::view('/how-we-do-it', 'pages.how-we-do-it')->name('why.how-we-do-it');
    Route::view('/industries-we-work', 'pages.industries-we-work')->name('why.industries-we-work');
    Route::view('/why-choose-smartchain', 'pages.why-choose-smartchain')->name('why.why-choose-smartchain');

    Route::view('/about-us', 'pages.about')->name('about-us');

    Route::view('/insights', 'pages.insights')->name('insights');
    Route::view('/collaboration-automation', 'pages.insight-page1')->name('insight.page1');
    Route::view('/demand-planning', 'pages.insight-page2')->name('insight.page2');

    Route::view('/contact-us', 'pages.contact-us')->name('contact-us');

    Route::view('/terms-of-use', 'pages.terms')->name('terms-of-use');
    Route::view('/privacy-policy', 'pages.privacy')->name('privacy-policy');
    Route::view('/cookie-policy', 'pages.cookies')->name('cookie-policy');
});
