!function (e) {
    "use strict";
    e(window).on("scroll", (function () {
        var s;
        !function () {
            var s = e(".intro-header");
            if (s.length) {
                var a = e(window).scrollTop(), t = e(".hamburger");
                a >= 1 ? (s.addClass("page-header--fixed"), t.removeClass("hamburger--white")) : (s.removeClass("page-header--fixed"), t.addClass("hamburger--white"))
            }
        }(), (s = e(".page-header")).length && (e(window).scrollTop() >= 1 ? s.addClass("page-header--fixed") : s.removeClass("page-header--fixed")), function () {
            var s = e(".page-header_2");
            if (s.length) {
                var a = e(window).scrollTop(), t = e(".main-menu"), i = e(".hamburger"), o = e(".lang-select");
                a >= 1 ? (s.addClass("page-header--fixed"), t.removeClass("main-menu--white"), i.removeClass("hamburger--white"), o.removeClass("lang-select--white")) : (s.removeClass("page-header--fixed"), t.addClass("main-menu--white"), i.addClass("hamburger--white"), o.addClass("lang-select--white"))
            }
        }(), function () {
            var s = e(".page-header_3");
            if (s.length) {
                var a = e(window).scrollTop(), t = e(".main-menu"), i = e(".hamburger"), o = e(".lang-select");
                a >= 1 ? (s.addClass("page-header--fixed"), t.removeClass("main-menu--white"), i.removeClass("hamburger--white"), o.removeClass("lang-select--white")) : (s.removeClass("page-header--fixed"), t.addClass("main-menu--white"), i.addClass("hamburger--white"), o.addClass("lang-select--white"))
            }
        }(), function () {
            var s = e(".page-header_4");
            s.length && (e(window).scrollTop() >= 1 ? s.addClass("page-header--fixed") : s.removeClass("page-header--fixed"))
        }(), function () {
            var s = e(".shop-header");
            s.length && (e(window).scrollTop() >= 1 ? s.addClass("shop-header--fixed") : s.removeClass("shop-header--fixed"))
        }(), function () {
            var s = e(".page-header_6");
            s.length && (e(window).scrollTop() >= 1 ? s.addClass("page-header--fixed") : s.removeClass("page-header--fixed"))
        }()
    })), e(document).ready((function () {
        var s, a, t, i, o;
        e("#ajax-form").length && e("#ajax-form").validate({
            rules: {
                name: {required: !0, minlength: 2},
                email: {required: !0, email: !0},
                phone: {required: !0},
                message: {required: !0}
            },
            messages: {
                name: {
                    required: "Please enter your name",
                    minlength: "Your name must consist of at least 2 characters"
                },
                email: {required: "Please enter your email"},
                phone: {required: "Please enter your phone number"},
                message: {required: "Please enter your message"}
            },
            submitHandler: function (s) {
                e(s).ajaxSubmit({
                    type: "POST", data: e(s).serialize(), url: "form.php", success: function () {
                        e(".alert--success").fadeIn(), e("#ajax-form").each((function () {
                            this.reset()
                        }))
                    }, error: function () {
                        e(".alert--error").fadeIn()
                    }
                })
            }
        }), function () {
            var s = e(".hamburger");
            if (s.length) {
                var a = e(".menu-dropdown__close"), t = e(".menu-dropdown--intro .screen__link");
                s.on("click", (function () {
                    e(this).toggleClass("hamburger--active"), e("body").toggleClass("body--static"), e(".menu-dropdown").toggleClass("menu-dropdown--active")
                })), a.on("click", (function () {
                    s.removeClass("hamburger--active"), e("body").removeClass("body--static"), e(".menu-dropdown").removeClass("menu-dropdown--active")
                })), t.on("click", (function () {
                    s.removeClass("hamburger--active"), e("body").removeClass("body--static"), e(".menu-dropdown").removeClass("menu-dropdown--active")
                }))
            }
        }(), e(".screen--trigger").on("click", (function () {
            var s = e(this).data("category");
            e(".screen--start").addClass("screen--inactive"), e(".menu-dropdown__inner").each((function () {
                e(this).data("value") === s && e(this).addClass("menu-dropdown__inner--active")
            }))
        })), e(".screen__back").on("click", (function () {
            e(".menu-dropdown__inner").removeClass("menu-dropdown__inner--active"), e(".screen--start").removeClass("screen--inactive")
        })), e(".accordion").length && e(".accordion__title-block").on("click", (function () {
            e(this).children(".accordion__close").toggleClass("accordion__close--active"), e(this).parents().children(".accordion__text-block").stop().slideToggle(300)
        })), e(".alert__close").on("click", (function () {
            e(this).parent(".alert").fadeOut(300)
        })), function () {
            var s = e(".js-counter");
            s.length && s.counterUp({delay: 10, time: 1e3})
        }(), function () {
            var s = e(".tabs");
            s.length && s.responsiveTabs({startCollapsed: "false"})
        }(), (s = e(".tracking-form__input")).length && s.on("change", (function () {
            var s = e(this);
            "" !== s.val() ? s.addClass("field--filled") : s.removeClass("field--filled")
        })), function () {
            var s = e(".form__select");
            s.length && s.niceSelect()
        }(), (a = e(".js-video")).length && a.fancybox(), function () {
            var s = e(".promo-slider");
            if (s.length) {
                var a = e(".promo-slider__count");
                s.on("init afterChange", (function (e, s, t, i) {
                    var o = (t || 0) + 1;
                    a.html(o + "<b>/</b>" + s.slideCount)
                })), s.slick({
                    fade: !0,
                    adaptiveHeight: !0,
                    infinite: !0,
                    speed: 1200,
                    arrows: !1,
                    dots: !0,
                    appendDots: e(".promo-slider__nav")
                })
            }
        }(),
            function () {
                var s = e(".promo-mainslider");
                if (s.length) {
                    var a = e(".promo-subslider");
                    s.slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        fade: !0,
                        adaptiveHeight: !0,
                        infinite: !0,
                        speed: 1200,
                        arrows: !1,
                        asNavFor: ".promo-subslider"
                    }), a.slick({
                        arrows: !1,
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        asNavFor: ".promo-mainslider",
                        focusOnSelect: !0
                    })
                }
            }(),
            function () {
                var s = e(".vertical-slider");
                var r = e(".mini-icon");

                if (s.length) {
                    s.slick({
                        vertical: true,
                        slidesToShow: 1.,
                        slidesToScroll: 1,
                        centerMode: true,
                        verticalSwiping: true,
                        autoplay: true,
                        autoplaySpeed: 5000,
                        dots: true,
                        arrows: false,
                    })
                    r.on('mouseenter', function () {
                        s.slick('slickPause');
                    })
                    r.on('mouseleave', function () {
                        s.slick('slickPlay');
                    })
                    r.click(function () {
                        s.slick('slickGoTo', e(this).data('index'));
                    })

                }
            }(),
            function () {
                var s = e(".home-slider");
                if (s.length) {
                    s.slick({})
                }
            }(),
        (t = e(".testimonials-slider")).length && t.slick({
            arrows: !1,
            dots: !0,
            adaptiveHeight: !0,
            appendDots: e(".testimonials__nav")
        }), function () {
            var s = e(".articles-slider");
            s.length && s.slick({
                arrows: true,
                prevArrow: $('.prev'),
                nextArrow: $('.next'),
                dots: false,
                slidesToShow: 3,
                responsive: [{breakpoint: 1200, settings: {slidesToShow: 2}}, {
                    breakpoint: 768,
                    settings: {slidesToShow: 1}
                }]
            })
        }(), function () {
            var s = e(".logos-slider");
            s.length && s.slick({
                slidesToShow: 4,
                arrows: !1,
                dots: !0,
                responsive: [{breakpoint: 992, settings: {slidesToShow: 3}}, {
                    breakpoint: 768,
                    settings: {slidesToShow: 2}
                }, {breakpoint: 576, settings: {slidesToShow: 1}}]
            })
        }(), function () {
            var s = e(".images-slider");
            s.length && s.slick({
                arrows: !1,
                dots: !0,
                slidesToShow: 2,
                appendDots: e(".images-slider__nav"),
                responsive: [{breakpoint: 768, settings: {slidesToShow: 1}}]
            })
        }(), function () {
            if (e(".js-range").length) {
                e(".range--calculator").ionRangeSlider({min: 0, max: 450, from: 234, skin: "round", step: 1});
                var s = e(".range--shop"), a = e(".range-slider__min"), t = e(".range-slider__max");
                s.ionRangeSlider({
                    skin: "round",
                    type: "double",
                    step: 5,
                    min: 0,
                    max: 70,
                    from: 5,
                    to: 55,
                    onChange: function (e) {
                        a.val(e.from), t.val(e.to)
                    }
                })
            }
        }(), function () {
            var s = e(".js-gallery");
            if (s.length) {
                s.isotope({itemSelector: ".js-gallery__item", percentPosition: !0});
                var a = e(".filter-panel__item");
                a.on("click", (function () {
                    var a = e(this).attr("data-filter");
                    s.isotope({filter: a})
                })), a.on("click", (function () {
                    a.removeClass("filter-panel__item--active"), e(this).addClass("filter-panel__item--active")
                }))
            }
        }(), function () {
            var s = e(".history--slider");
            s.length && s.slick({
                slidesToShow: 3.5,
                infinite: false,
                arrows: !1,
                dots: !0,
                appendDots: e(".steps-slider__nav"),
                responsive: [{breakpoint: 1200, settings: {slidesToShow: 2}}, {
                    breakpoint: 768,
                    settings: {slidesToShow: 1}
                }]
            })
        }(), function () {
            var s = e(".cooperation--slider");
            s.length && s.slick({
                slidesToShow: 4,
                arrows: !1,
                responsive: [{breakpoint: 1200, settings: {slidesToShow: 3}}, {
                    breakpoint: 992,
                    settings: {slidesToShow: 2}
                }, {breakpoint: 576, settings: {slidesToShow: 1}}]
            })
        }(), function () {
            var s = e(".cases-slider");
            if (s.length) {
                var a = e(".cases-slider__nav");
                s.slick({
                    speed: 600,
                    centerMode: !0,
                    slidesToShow: 3,
                    centerPadding: "0px",
                    arrows: !1,
                    dots: !0,
                    appendDots: a,
                    responsive: [{breakpoint: 1200, settings: {slidesToShow: 2, centerMode: !1}}, {
                        breakpoint: 768,
                        settings: {slidesToShow: 1}
                    }]
                })
            }
        }(), e('[data-toggle="datepicker"]').datepicker(), function () {
            if (e(".cart-item__count").length) {
                var s = e(".cart-item__minus"), a = e(".cart-item__plus");
                s.on("click", (function () {
                    var s = e(this).parent().find("input"), a = parseInt(s.val()) - 1;
                    return a = a < 1 ? 1 : a, s.val(a), s.change(), !1
                })), a.on("click", (function () {
                    var s = e(this).parent().find("input");
                    return s.val(parseInt(s.val()) + 1), s.change(), !1
                }))
            }
        }(), (i = e(".password-trigger")).length && i.on("click", (function () {
            e(this).toggleClass("password-trigger--active");
            var s = e(e(this).attr("toggle"));
            "password" == s.attr("type") ? s.attr("type", "text") : s.attr("type", "password")
        })), function () {
            var s = e(".bests-slider");
            if (s.length) {
                var a = e(".bests-slider__nav");
                s.slick({
                    slidesToShow: 4,
                    arrows: !1,
                    dots: !0,
                    appendDots: a,
                    responsive: [{breakpoint: 992, settings: {slidesToShow: 3}}, {
                        breakpoint: 768,
                        settings: {slidesToShow: 2}
                    }, {breakpoint: 576, settings: {slidesToShow: 1}}]
                })
            }
        }(), function () {
            var s = e(".main-slider");
            s.length && (s.slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: !1,
                asNavFor: ".nav-slider",
                fade: !0
            }), e(".nav-slider").slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                asNavFor: ".main-slider",
                focusOnSelect: !0,
                arrows: !1,
                vertical: !0,
                responsive: [{breakpoint: 1200, settings: {slidesToShow: 3}}, {
                    breakpoint: 576,
                    settings: {vertical: !1, slidesToShow: 3}
                }]
            }))
        }(), function () {
            if (e(".form__count").length) {
                var s = e(".form__minus"), a = e(".form__plus");
                s.on("click", (function () {
                    var s = e(this).parent().find("input"), a = parseInt(s.val()) - 1;
                    return a = a < 1 ? 1 : a, s.val(a), s.change(), !1
                })), a.on("click", (function () {
                    var s = e(this).parent().find("input");
                    return s.val(parseInt(s.val()) + 1), s.change(), !1
                }))
            }
        }(), (o = e(".js-scroll")).length && o.mPageScroll2id({highlightClass: "js-scroll--highlighted"}), function () {
            var s = e(".pages-slider");
            if (s.length) {
                var a = e(".pages-slider__nav");
                s.slick({
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    arrows: !1,
                    dots: !0,
                    appendDots: a,
                    responsive: [{breakpoint: 992, settings: {slidesToShow: 2, slidesToScroll: 2}}, {
                        breakpoint: 576,
                        settings: {slidesToShow: 1, slidesToScroll: 1}
                    }]
                })
            }
        }(), function () {
            var s = e(".shop__aside-trigger");
            s.length && (s.on("click", (function () {
                e(".aside-holder").addClass("aside-holder--visible"), e(".shop__backdrop").addClass("shop__backdrop--visible"), e("body").addClass("body--static")
            })), e(".shop__aside-close").on("click", (function () {
                e(".aside-holder").removeClass("aside-holder--visible"), e(".shop__backdrop").removeClass("shop__backdrop--visible"), e("body").removeClass("body--static")
            })), e(".shop__backdrop").on("click", (function () {
                e(this).removeClass("shop__backdrop--visible"), e(".aside-holder").removeClass("aside-holder--visible"), e("body").removeClass("body--static")
            })))
        }(), function () {
            var el = e(".icon");
            el.replaceWith(function () {
                return $("<span class='icon'/>", {html: $(this).html()});
            });
        }()
    }))
}(jQuery);
