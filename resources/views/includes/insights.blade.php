<section class="section bg--lgray front-blog">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 d-flex flex-column align-items-start">
                <div class="heading"><span class="heading__pre-title">Insights</span>
                    <h3 class="heading__title">Our latest <span class="color--green">Insights</span></h3>
                    <span class="heading__layout layout--lgray">Blog</span>
                </div>
                <a class="d-none d-xl-inline-block button button--green" href="{{route('insights')}}"><span>View more</span>

                </a>
                <div class="articles-slider__nav">
                    <div class="prev nav-arrow"><ion-icon name="arrow-back"></ion-icon></div>
                    <div class="next nav-arrow"><ion-icon name="arrow-forward"></ion-icon></div>
                </div>
            </div>
            <div class="col-xl-8">
                <div class="articles-slider-wrapper">
                    <div class="articles-slider">
                        <div class="articles-slider__item">
                            <div class="article">
                                <div class="article__img"><img class="img--bg" src="{{asset('img/site/blog_1.webp')}}" alt="img"/>
                                </div>
                                <div class="article__lower">
                                    <h6 class="article__title"><a href="{{route('insight.page1')}}">
                                            7 easy steps to successfully implement forecast
                                            Collaboration Automation
                                        </a></h6>
                                    <p class="article__text">
                                        In today’s world of business, it is impossible to overlook the importance of the
                                        supply chain. It has
                                        clearly transformed from a supportive role into one of the most important business
                                        functions as well...
                                    </p>
                                    <div class="article__details">
                                        <span>20 January, 2023</span></div>
                                </div>
                            </div>
                        </div>
                        <div class="articles-slider__item">
                            <div class="article">
                                <div class="article__img"><img class="img--bg" src="{{asset('img/site/blog_2.webp')}}"
                                                               alt="img"/></div>
                                <div class="article__lower">
                                    <h6 class="article__title"><a href="{{route('insight.page2')}}">  The obstacles and challenges in
                                            demand planning, and how to
                                            address them</a></h6>
                                    <p class="article__text">
                                        When you produce too much, too early, there is a danger of products accumulating in your
                                        warehouses for a long time before you can send them to your customers. On the other
                                        hand, when you produce too little too late...
                                    </p>
                                    <div class="article__details">
                                        <span>02 December, 2022</span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row top-50 d-block d-xl-none">
                    <div class="col-12 text-center"><a class="button button--green"
                                                       href="{{route('insights')}}"><span>View more</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
