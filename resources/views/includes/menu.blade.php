<div class="container">
    <div class="row align-items-center">
        <div class="col-8 col-md-6 col-lg-3 d-flex align-items-center">
            <div class="hamburger d-none d-lg-inline-block ">
                <div class="hamburger-inner"></div>
            </div>
            @if(request()->routeIs('home'))
                <div class="page-header__logo logo--white">
                    <a href="{{route('home')}}">
                        <img src="{{asset('img/logo/logo-light.svg')}}" alt="logo"/>
                    </a>
                </div>
                <div class="page-header__logo logo--dark">
                    <a href="{{route('home')}}">
                        <img src="{{asset('img/logo/logo-dark.svg')}}" alt="logo"/>
                    </a>
                </div>
            @else
                <div class="page-header__logo">
                    <a href="{{route('home')}}">
                        <img src="{{asset('img/logo/logo-dark.svg')}}" alt="logo"/>
                    </a>
                </div>
            @endif
        </div>
        <div class="col-lg-7 d-none d-lg-block">
            <!-- main menu start-->
            <ul class="main-menu @if(request()->routeIs('home')) main-menu--white @endif">
                <li class="main-menu__item main-menu__item--has-child @if(request()->routeIs('services.*')) main-menu__item--active @endif">
                    <a class="main-menu__link" href="javascript:void(0);"><span>Our Services</span></a>
                    <!-- sub menu start-->
                    <ul class="main-menu__sub-list">
                        <li @if(request()->routeIs('services.business-advisory-service')) class="item--active" @endif>
                            <a href="{{route('services.business-advisory-service')}}">
                                <span>Business Consulting Service</span>
                            </a>
                        </li>
                        <li @if(request()->routeIs('services.implementation-service')) class="item--active" @endif>
                            <a href="{{route('services.implementation-service')}}">
                                <span>Implementation Service</span>
                            </a>
                        </li>
                        <li @if(request()->routeIs('services.technology-advisory-service')) class="item--active" @endif>
                            <a href="{{route('services.technology-advisory-service')}}">
                                <span>Technology Consulting Service</span>
                            </a>
                        </li>
                    </ul>
                    <!-- sub menu end-->
                </li>
                <li class="main-menu__item main-menu__item--has-child @if(request()->routeIs('solution.*')) main-menu__item--active @endif">
                    <a class="main-menu__link" href="javascript:void(0);"><span>Our Solution</span></a>
                    <!-- sub menu start-->
                    <ul class="main-menu__sub-list">
                        <li @if(request()->routeIs('solution.supply-chain-planning')) class="item--active" @endif>
                            <a href="{{route('solution.supply-chain-planning')}}">
                                <span>Supply Chain Planning</span>
                            </a>
                        </li>
                        <li @if(request()->routeIs('solution.supply-chain-execution')) class="item--active" @endif>
                            <a href="{{route('solution.supply-chain-execution')}}">
                                <span>Supply Chain Execution</span>
                            </a>
                        </li>
                    </ul>
                    <!-- sub menu end-->
                </li>
                <li class="main-menu__item main-menu__item--has-child @if(request()->routeIs('why.*')) main-menu__item--active @endif">
                    <a class="main-menu__link" href="javascript:void(0);"><span>Why Smartchain</span></a>
                    <!-- sub menu start-->
                    <ul class="main-menu__sub-list">
                        <li @if(request()->routeIs('why.how-we-do-it')) class="item--active" @endif>
                            <a href="{{route('why.how-we-do-it')}}">
                                <span>How we do it?</span>
                            </a>
                        </li>
                        <li @if(request()->routeIs('why.industries-we-work')) class="item--active" @endif>
                            <a href="{{route('why.industries-we-work')}}">
                                <span>Industries we work</span>
                            </a>
                        </li>
                        <li @if(request()->routeIs('why.why-choose-smartchain')) class="item--active" @endif>
                            <a href="{{route('why.why-choose-smartchain')}}">
                                <span>Why choose SmartChain?</span>
                            </a>
                        </li>
                    </ul>
                    <!-- sub menu end-->
                </li>
                <li class="main-menu__item @if(request()->routeIs('about-us')) main-menu__item--active @endif">
                    <a class="main-menu__link" href="{{route('about-us')}}">
                        <span>About Us</span>
                    </a>
                </li>
                <li class="main-menu__item @if(request()->routeIs('insights')) main-menu__item--active @endif">
                    <a class="main-menu__link" href="{{route('insights')}}">
                        <span>Insights</span>
                    </a>
                </li>
            </ul>
            <!-- main menu end-->
        </div>
        <div class="col-3 col-md-6 col-lg-2 d-flex justify-content-end align-items-center">
            <a class="button button--filled" href="{{route('contact-us')}}">Get in Touch</a>
            <!-- menu-trigger start-->
            <div class="hamburger d-inline-block d-lg-none">
                <div class="hamburger-inner"></div>
            </div>
            <!-- menu-trigger end-->
        </div>
    </div>
</div>
