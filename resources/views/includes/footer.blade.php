<!-- footer start-->
<footer class="page-footer footer_2">
    <img class="section--bg b0 r0" src="{{asset('img/footer-bg.png')}}" alt="bg">
    <img class="bg-icon" src="{{asset('img/logo/icon-color.svg')}}" alt="bg">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 col-lg-4">
                <div class="page-footer__logo">
                    <a href="{{route('home')}}">
                        <img src="{{asset('img/logo/logo-default.svg')}}" alt="logo">
                    </a>
                </div>

            </div>
            <div class="col-md-3 col-lg-4 top-30 top-md-0">
                <h6 class="page-footer__title title--white">Address</h6>
                <div class="page-footer__details">
                    <p><span>12, Scott Court, <br>
                            Chester Brook, PA 19087</span></p>
                </div>
            </div>
            <div class="col-lg-4 top-30 top-lg-0">
                <h6 class="page-footer__title title--white">Stay in Touch</h6>
                <form class="form newslatter-form" action="javascript:void(0);">
                    <div class="fieldset">
                        <input class="form__field" type="email" name="email" placeholder="Email address">
                        <button class="form__submit text-white" type="submit">
                            <ion-icon size="large" name="send"></ion-icon>
                        </button>
                    </div>
                    <ul class="socials list--reset">
                        {{--                        <li class="socials__item"><a class="socials__link" href="#">--}}
                        {{--                                <ion-icon name="logo-youtube"></ion-icon>--}}
                        {{--                            </a></li>--}}
                        {{--                        <li class="socials__item"><a class="socials__link" href="#">--}}
                        {{--                                <ion-icon name="logo-facebook"></ion-icon>--}}
                        {{--                            </a></li>--}}
                        {{--                        <li class="socials__item"><a class="socials__link" href="#">--}}
                        {{--                                <ion-icon name="logo-twitter"></ion-icon>--}}
                        {{--                            </a></li>--}}
                        <li class="socials__item"><a class="socials__link" target="_blank"
                                                     href="https://www.linkedin.com/company/smartchain-supplychain-solutions/">
                                <ion-icon name="logo-linkedin"></ion-icon>
                            </a></li>
                        {{--                        <li class="socials__item"><a class="socials__link" href="#">--}}
                        {{--                                <ion-icon name="logo-instagram"></ion-icon>--}}
                        {{--                            </a></li>--}}
                    </ul>
                </form>
            </div>
        </div>
        <hr class="">
        <div class="row top-50">
            <div class="col-sm-12 col-lg-6 top-20 top-sm-0">
                <div class="page-footer__copyright">© {{date('Y')}} {{config('app.name')}}. All rights reserved</div>
            </div>
            <div
                class="col-sm-12 col-lg-6 mt-md-3 mt-lg-0">
                <div class="page-footer__privacy d-lg-block text-lg-right">
                    <a href="{{route('terms-of-use')}}">Terms of Use</a>
                    <a href="{{route('privacy-policy')}}">Privacy policy</a>
                    <a href="{{route('cookie-policy')}}">Cookies Policy</a></div>
            </div>
        </div>
    </div>
</footer>
<!-- footer end-->
