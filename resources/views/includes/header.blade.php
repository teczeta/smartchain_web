<!-- menu dropdown start-->
<div class="menu-dropdown">
    <div class="menu-dropdown__inner" data-value="start">
        <div class="screen screen--start">
            <div class="menu-dropdown__close">
                <ion-icon name="close"></ion-icon>
            </div>
            <div class="d-block d-lg-none bottom-20">
                <img height="200px" src="{{asset('img/logo/logo-default.svg')}}" alt="">
            </div>
            <div class="d-block d-lg-none bottom-20">
                <div class="screen__item screen--trigger @if(request()->routeIs('services.*')) item--active @endif"
                     data-category="screen-services">
                    <span>Our Services</span>
                    <span><ion-icon name="arrow-forward"></ion-icon></span>
                </div>

                <div class="screen__item screen--trigger @if(request()->routeIs('solution.*')) item--active @endif"
                     data-category="screen-solution">
                    <span>Our Solution</span>
                    <span><ion-icon name="arrow-forward"></ion-icon></span>
                </div>

                <div class="screen__item screen--trigger @if(request()->routeIs('why.*')) item--active @endif"
                     data-category="screen-why">
                    <span>Why Smartchain?</span>
                    <span><ion-icon name="arrow-forward"></ion-icon></span>
                </div>

                <div class="screen__item @if(request()->routeIs('about-us')) item--active @endif">
                    <a class="text-reset text-decoration-none" href="{{route('about-us')}}">About Us</a>
                </div>

                <div class="screen__item @if(request()->routeIs('insights')) item--active @endif">
                    <a class="text-reset text-decoration-none" href="{{route('insights')}}">Insights</a>
                </div>
            </div>

            <div class="menu-dropdown__block top-50">
                <span class="block__title">Address</span>
               <span class="text-white">12, Scott Court, Chester Brook, PA 19087</span>
            </div>
           <div class="menu-dropdown__block">
                <ul class="socials list--reset">
{{--                    <li class="socials__item">--}}
{{--                        <a class="socials__link" href="#">--}}
{{--                            <ion-icon name="logo-youtube"></ion-icon>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="socials__item">--}}
{{--                        <a class="socials__link" href="#">--}}
{{--                            <ion-icon name="logo-facebook"></ion-icon>--}}
{{--                        </a>--}}
{{--                    </li>--}}
{{--                    <li class="socials__item">--}}
{{--                        <a class="socials__link" href="#">--}}
{{--                            <ion-icon name="logo-twitter"></ion-icon>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                    <li class="socials__item">
                        <a class="socials__link" href="https://www.linkedin.com/company/smartchain-supplychain-solutions/" target="_blank">
                            <ion-icon name="logo-linkedin"></ion-icon>
                        </a>
                    </li>
{{--                    <li class="socials__item">--}}
{{--                        <a class="socials__link" href="#">--}}
{{--                            <ion-icon name="logo-instagram"></ion-icon>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                </ul>
            </div>
            <div class="menu-dropdown__block top-50">
                <a class="button button--filled side-btn" href="{{route('contact-us')}}">Get in Touch</a>
            </div>
        </div>
    </div>
    <div class="menu-dropdown__inner" data-value="screen-services">
        <div class="screen screen--sub">
            <div class="screen__heading">
                <h6 class="screen__back">
                    <ion-icon name="arrow-back"></ion-icon>
                    <span>Our Services</span>
                </h6>
            </div>
            <div class="screen__item @if(request()->routeIs('services.business-advisory-service')) item--active @endif">
                <a class="screen__link" href="{{route('services.business-advisory-service')}}">
                    Business Consulting Service
                </a>
            </div>
            <div class="screen__item @if(request()->routeIs('services.implementation-service')) item--active @endif">
                <a class="screen__link" href="{{route('services.implementation-service')}}">
                    Implementation Service
                </a>
            </div>
            <div
                class="screen__item @if(request()->routeIs('services.technology-advisory-service')) item--active @endif">
                <a class="screen__link" href="{{route('services.technology-advisory-service')}}">
                    Technology Consulting Service
                </a>
            </div>
        </div>
    </div>

    <div class="menu-dropdown__inner" data-value="screen-solution">
        <div class="screen screen--sub">
            <div class="screen__heading">
                <h6 class="screen__back">
                    <ion-icon name="arrow-back"></ion-icon>
                    <span>Our Solution</span>
                </h6>
            </div>
            <div class="screen__item @if(request()->routeIs('solution.supply-chain-planning')) item--active @endif">
                <a class="screen__link" href="{{route('solution.supply-chain-planning')}}">
                    Supply Chain Planning
                </a>
            </div>
            <div class="screen__item @if(request()->routeIs('solution.supply-chain-execution')) item--active @endif">
                <a class="screen__link" href="{{route('solution.supply-chain-execution')}}">
                    Supply Chain Execution
                </a>
            </div>
        </div>
    </div>

    <div class="menu-dropdown__inner" data-value="screen-why">
        <div class="screen screen--sub">
            <div class="screen__heading">
                <h6 class="screen__back">
                    <ion-icon name="arrow-back"></ion-icon>
                    <span>Why Smartchain</span>
                </h6>
            </div>
            <div class="screen__item @if(request()->routeIs('why.how-we-do-it')) item--active @endif">
                <a class="screen__link" href="{{route('why.how-we-do-it')}}">
                    How we do it?
                </a>
            </div>
            <div class="screen__item @if(request()->routeIs('why.industries-we-work')) item--active @endif">
                <a class="screen__link" href="{{route('why.industries-we-work')}}">
                    Industries we work
                </a>
            </div>
            <div class="screen__item @if(request()->routeIs('why.why-choose-smartchain')) item--active @endif">
                <a class="screen__link" href="{{route('why.why-choose-smartchain')}}">
                    Why choose SmartChain?
                </a>
            </div>
        </div>
    </div>

</div>
<!-- menu dropdown end-->

@if(request()->routeIs('home'))
    <!-- header start-->
    <header class="page-header_2">
        <div class="page-header__top d-none d-xl-block">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-9">
                    <span>
                        <ion-icon class="mr-1" name="pin"></ion-icon>
                    12, Scott Court, Chester Brook, PA 19087
                    </span>
                    </div>
                   {{-- <div class="col-xl-3 text-right">
                        <ul class="socials list--reset">
                            <li class="socials__item"><a class="socials__link" href="#">
                                    <ion-icon name="logo-youtube"></ion-icon>
                                </a></li>
                            <li class="socials__item"><a class="socials__link" href="#">
                                    <ion-icon name="logo-facebook"></ion-icon>
                                </a></li>
                            <li class="socials__item"><a class="socials__link" href="#">
                                    <ion-icon name="logo-twitter"></ion-icon>
                                </a></li>
                            <li class="socials__item"><a class="socials__link" href="#">
                                    <ion-icon name="logo-linkedin"></ion-icon>
                                </a></li>
                            <li class="socials__item"><a class="socials__link" href="#">
                                    <ion-icon name="logo-instagram"></ion-icon>
                                </a></li>
                        </ul>
                    </div>--}}
                </div>
            </div>
        </div>
        <div class="page-header__lower">
            @include('includes.menu')
        </div>
    </header>
    <!-- header end-->
@else
    <header class="page-header">
        @include('includes.menu')
    </header>
@endif

