@extends('layouts.app')

@section('content')
    <main class="main">

        <!-- section start-->
        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">Insights</h1>
                                <h5 class="text-white mt-3">
                                    Our latest insights and white papers
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end-->


        <section class="section blog">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-xl-9">
                        <div class="row">
                            <div class="col-md-6 col-xl-4">
                                <div class="blog-item">
                                    <div class="blog-item__img"><img class="img--bg" src="{{asset('img/site/blog_1.webp')}}" alt="img"/>
                                    </div>
                                    <span class="blog-item__category">Business</span>
                                    <h6 class="blog-item__title"><a href="{{route('insight.page1')}}"> 7 easy steps to successfully implement forecast
                                            Collaboration Automation</a></h6>
                                    <div class="blog-item__text">
                                        In today’s world of business, it is impossible to overlook the importance of the
                                        supply chain. It has
                                        clearly transformed from a supportive role into one of the most important business
                                        functions as well...
                                    </div>
                                    <div class="blog-item__details">
                                        <span>20 January 2023</span></div>
                                </div>
                            </div>
                            <div class="col-md-6 col-xl-4">
                                <div class="blog-item">
                                    <div class="blog-item__img"><img class="img--bg" src="{{asset('img/site/blog_2.webp')}}" alt="img"/>
                                    </div>
                                    <span class="blog-item__category">Business</span>
                                    <h6 class="blog-item__title"><a href="{{route('insight.page2')}}">The obstacles and challenges in
                                            demand planning, and how to
                                            address them</a></h6>
                                    <div class="blog-item__text">When you produce too much, too early, there is a danger of products accumulating in your
                                        warehouses for a long time before you can send them to your customers. On the other
                                        hand, when you produce too little too late...
                                    </div>
                                    <div class="blog-item__details">
                                        <span>02 December 2022</span></div>
                                </div>
                            </div>

                        </div>
                        {{--<div class="row">
                            <div class="col-12">
                                <ul class="pagination list--reset">
                                    <li class="pagination__item pagination__item--prev"><span>Back</span></li>
                                    <li class="pagination__item pagination__item--active"><span>1</span></li>

                                    <li class="pagination__item pagination__item--next"><span>Next</span></li>
                                </ul>
                            </div>
                        </div>--}}
                    </div>
                    <div class="col-lg-4 col-xl-3 top-70 top-lg-0">
                        <div class="row">
                            <div class="col-md-6 col-lg-12 bottom-50">
                                <h5 class="blog__title">Categories</h5>
                                <ul class="category-list list--reset">
                                    <li class="category-list__item item--active"><a class="category-list__link"
                                                                                    href="#"><span>Insights</span></a>
                                    </li>
                                    <li class="category-list__item"><a class="category-list__link" href="#"><span>WhitePapers</span></a>
                                    </li>
                                </ul>
                            </div>


                            <div class="col-md-6 col-lg-12">
                                <div class="contact-trigger contact-trigger--style-2"><img class="contact-trigger__img"
                                                                                           src="{{asset('img/contact_background.png')}}"
                                                                                           alt="img"/>
                                    <h4 class="contact-trigger__title">How we can help you!</h4>
                                    <p class="contact-trigger__text">
                                        Get in touch
                                    </p><a class="button button--white"
                                                                 href="{{route('contact-us')}}"><span>Contact us</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section p-5 bg--lgray">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-lg-7">
                        <div class="heading">
                            <h3 class="heading__title">Get in <span
                                    class="color--green">touch</span></h3>
                        </div>

                        <p class="mt-4">
                            Contact us today to learn more about how the SmartChain team can help your business optimize
                            its supply chain operations.
                        </p>

                    </div>
                    <div class="col-lg-5 text-right">
                        <a class="button button--filled" href="{{route('contact-us')}}"><span>Contact us</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>


    </main>
@endsection
