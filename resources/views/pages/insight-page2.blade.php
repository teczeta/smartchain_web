@extends('layouts.app')

@section('content')
    <main class="main">

        <!-- section start-->
        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">Insight</h1>
                                <h5 class="text-white mt-3">
                                    The obstacles and challenges in
                                    demand planning, and how to
                                    address them
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end-->


        <section class="section blog">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-xl-9">
                        <div class="row">
                            <div class="col-12"><img class="blog-post__img" src="{{asset('img/site/blog_2.webp')}}"
                                                     alt="img"/>
                                <h4 class="blog-post__title">The obstacles and challenges in
                                    demand planning, and how to
                                    address them</h4>
                                <p>
                                    When you produce too much, too early, there is a danger of products accumulating in
                                    your
                                    warehouses for a long time before you can send them to your customers. On the other
                                    hand, when you produce too little too late, you will not be able to meet the demand.
                                    However, producing the right amount of things at the right time is not easy, as the
                                    market is
                                    not static; demand rises and falls quickly in changing market conditions. During
                                    those times,
                                    you need to quickly ramp up the supply or scale down based on accurate estimates,
                                    which
                                    requires robust demand planning.
                                </p>
                                <h4>
                                    What is demand planning?
                                </h4>
                                <p>
                                    Demand planning is the process of anticipating the demand in the supply chain
                                    through
                                    accurate forecasts based on various indicators and data points and predicting the
                                    demand
                                    fluctuations. It is not easy, as you cannot get an accurate forecast overnight. It
                                    takes a lot of
                                    effort, investment, and a relentless focus toward identifying issues,
                                    troubleshooting them,
                                    and constantly seizing every opportunity to find improvements in your forecasts. It
                                    also
                                    requires introducing the latest technologies and automation that can pull all the
                                    relevant
                                    data points and combine them to provide accurate insights and projections on
                                    impending
                                    demand fluctuations.
                                    Demand planning has several obstacles, despite two decades of process and technology
                                    refinement in the supply chain. As a result, supply chain teams still struggle to
                                    achieve
                                    excellence in demand planning and management.
                                </p>
                                <h6>
                                    Following are some limitations and constraints in precise demand planning
                                </h6>
                                <p class=" mb-4">
                                    The complete set of data required for accurate demand planning is not available most
                                    of the
                                    time. Even if it is available, it is not necessarily accurate or up-to-date.
                                    The supply chain network is too complex that it requires a thorough analysis of
                                    various
                                    factors and parameters from multiple levels of the supply chain, to come up with an
                                    accurate prediction.
                                    There are too many stakeholders; it is hard to establish consistently good
                                    communication
                                    and achieve synergy and collaboration amongst all the stakeholders.
                                    The methods and systems used for forecasting can be inadequate.
                                    When the market conditions are unstable with erratic demand patterns and market
                                    trends,
                                    it is extremely difficult to forecast damand.
                                    One of the challenges with implementing better demand forecasting could be the lack
                                    of
                                    adoption of new technologies and resistance to change at various levels and parts of
                                    the
                                    supply chain.</p>
                                <p>
                                    Demand prediction may require many changes in your existing system, and you may have
                                    to
                                    bring in new technologies and automation. It can get expensive, and if you have
                                    limited
                                    resources and budget, it will be impossible to implement demand planning.
                                    Accurate demand forecasting requires transparency throughout the supply chain. When
                                    there is no/little visibility from end-to-end in the supply chain, it is impossible
                                    to forecast
                                    demand precisely.
                                    As a result of these factors, the current state of demand planning is still a work
                                    in progress
                                    at most organizations. In this article, we will analyze the situation and find ways
                                    and
                                    workarounds to make demand planning work.
                                </p>
                                <h6>
                                    Following are some limitations and constraints in precise demand planning
                                </h6>
                                <ul class="list list--check list--reset">
                                    <li class="list__item">The complete set of data required for accurate demand
                                        planning is not available most of the time. Even if it is available, it is not
                                        necessarily accurate or up-to-date.
                                    </li>
                                    <li class="list__item">The supply chain network is too complex that it requires a
                                        thorough analysis of various factors and parameters from multiple levels of the
                                        supply chain, to come up with an accurate prediction.
                                    </li>
                                    <li class="list__item">There are too many stakeholders; it is hard to establish
                                        consistently good communication and achieve synergy and collaboration amongst
                                        all the stakeholders.
                                    </li>
                                    <li class="list__item">The methods and systems used for forecasting can be
                                        inadequate.
                                    </li>
                                    <li class="list__item">When the market conditions are unstable with erratic demand
                                        patterns and market trends, it is extremely difficult to forecast damand.
                                    </li>
                                    <li class="list__item">One of the challenges with implementing better demand
                                        forecasting could be the lack of adoption of new technologies and resistance to
                                        change at various levels and parts of the supply chain.
                                    </li>
                                    <li class="list__item">Demand prediction may require many changes in your existing
                                        system, and you may have to bring in new technologies and automation. It can get
                                        expensive, and if you have limited resources and budget, it will be impossible
                                        to implement demand planning.
                                    </li>
                                    <li class="list__item"> 8. Accurate demand forecasting requires transparency
                                        throughout the supply chain. When there is no/little visibility from end-to-end
                                        in the supply chain, it is impossible to forecast demand precisely.
                                    </li>

                                </ul>
                                <p>As a result of these factors, the current state of demand planning is still a work in
                                    progress at most organizations. In this article, we will analyze the situation and
                                    find ways and workarounds to make demand planning work.</p>
                                <h5>
                                    The evolution of demand planning
                                </h5>
                                <p>
                                    The supply chain planning processes have evolved in the last four decades. What
                                    started once as isolated areas of improvement in various operations of the
                                    organization gradually moved towards a more integrated approach, where companies
                                    started planning the supply chain operations based on the data obtained from
                                    multiple levels in the complex supply chain and from different functional aspects
                                    and departments of the organization - from manufacturing to procurement, inventory,
                                    logistics, etc., to sales and marketing.
                                </p>
                                <p>
                                    Ideally, when the companies start tapping the relevant data points from every level
                                    of the supply chain and various operations of the organization, demand planning gets
                                    integrated into various processes, resulting in a truly end-to-end demand management
                                    process.</p>
                                <h5>The current state of the market</h5>
                                <p>The technologies behind demand planning have evolved over the years, but the curve
                                    has leveled in the last ten years, creating stagnation. The market has changed, and
                                    now the supply chain is more complicated than ever. As a result, the companies are
                                    struggling to go beyond a certain point in achieving efficiencies with the older
                                    technologies and methods, as shown in the table below. </p>
                                <p class="text-center">
                                    <img src="{{asset('img/blog-img-2.jpg')}}" alt="">
                                </p>
                                <p>Growth has slowed, inventories have increased, and costs have escalated. As a result,
                                    there is a need to get back to the basics and do things right to overcome this
                                    stagnation. A quick look at the year-over-year financial balance sheet results will
                                    reveal how supply chain limitations affect companies. </p>
                                <p>We have discussed the supply chain complexity and the limited visibility into the
                                    supplier inventory. But one of the most important yet least discussed pain points is
                                    demand volatility.</p>
                                <h5>The problem of demand volatility</h5>
                                <p>The reach of the products constanty increases as companies bring them to newer
                                    markets and countries. As a result, the demand gets more fragmented and varies with
                                    the market, requiring a focus on regional needs. It causes demand volatility, making
                                    it hard to forecast and predict the demand for products in the market overall.
                                    Demand forecasting in a traditional supply chain is based on month-old order and
                                    shipment data, which is inadequate for today’s needs. The significant latency this
                                    data comes with also becomes a problem. Making accurate demand forecasts requires
                                    real-time data. Consequently, the traditional supply chain is incapable of
                                    forecasting demand properly and is slow to respond to demand fluctuations when there
                                    is demand volatility.</p>
                                <p class="text-center">
                                    <img src="{{asset('img/blog-img-3.jpg')}}" alt="">
                                </p>
                                <p>It is essential to transform the traditional supply chain into a more adaptable
                                    system to address demand volatility. The supply chain needs to embody the
                                    contemporary, radical concepts of demand management.</p>
                                <p>Contemporary supply chain model addresses demand volatility by achieving better
                                    demand planning based on comprehensive analytics and insights gained out of various
                                    parameters such as:</p>
                                <ul class="list list--check list--reset">
                                    <li class="list__item">Historical sales data</li>
                                    <li class="list__item">Market trends and consumer behavior</li>
                                    <li class="list__item">Channel-wise demand data</li>
                                    <li class="list__item">Product lifecycle stage</li>
                                    <li class="list__item">Economic indicators (such as GDP, inflation, consumer
                                        confidence, etc.)
                                    </li>
                                    <li class="list__item">Competitor analysis</li>
                                    <li class="list__item">Seasonality and promotions</li>
                                    <li class="list__item">Marketing and sales plans</li>
                                    <li class="list__item">Customer orders and shipments</li>
                                    <li class="list__item">Customer feedback and surveys</li>
                                </ul>
                                <p>This demand planning model provides accurate insights. However, the accuracy of these
                                    insights relies on the lack of bias and error. Managing these challenges and
                                    implementing this demand management system requires the following.</p>

                                <h5>Step 1: Accept the shortfalls</h5>
                                <p>The first step in addressing demand volatility involves accepting the flaws and
                                    shortfalls in your current demand management model. It also requires identifying
                                    other related issues, such as using outdated and unsuited technologies and flawed
                                    practices.</p>
                                <p>
                                    It is essential to acknowledge the fact that demand management processes have
                                    stagnated in the last few years and recognize the need to move away from the
                                    mistakes committed during this era, including:</p>
                                <h6>5 key Limitations of current demand management processes:</h6>
                                <p>One-number forecasting: It is a method in which demand forecasting is done centrally
                                    for the whole organization and is considered universal for all functions and
                                    purposes. It does not consider the diversity of products, time, geographies,
                                    channels, and other attributes that make it necessary to take a hierarchical
                                    approach to demand forecasting.</p>
                                <p>For instance, a product may have multiple variants, each performing well in different
                                    markets. But forecasting the demand for the product as a whole and sending all the
                                    variants equally to all geographical locations, channels and markets will create a
                                    problem. One specific variant would sell faster than the others, and the
                                    less-preferred variants would remain on the shelf much longer. So the demand will
                                    not reflect in sales as the best-selling variant would go out of stock earlier</p>
                                <p>So, one-number forecasting limits the organization to base demand planning on a
                                    singular, centralized assessment of possibilities, assumptions, and risks. Instead,
                                    your demand planning should be based on the consensus of various operations,
                                    including marketing, sales, financial, and supply chain, instead of market
                                    assumptions. For that, you
                                    need more advanced, integrated forecasting systems.</p>
                                <p>Consensus planning: Consensus planning is the process of demand planning for the
                                    organization by involving stakeholders from different divisions, departments, and
                                    teams within the company. For instance, you can conduct workshops, discussions,
                                    surveys, etc., with representatives from sales, marketing, inventory, procurement,
                                    and supply chain teams from the residential, commercial, and industrial divisions of
                                    the same company, to bring diverse insights and make demand planning for the entire
                                    company better. However, in practice, it does not work to improve the forecast
                                    accuracy mainly because of the biases and errors that each of those groups
                                    carries.</p>
                                <p> 3. Forecasting based on production data: Companies that follow the traditional
                                    demand planning method look into the production data and trends to forecast what
                                    they should produce and how much they need to produce. However, the time taken from
                                    production to sale is quite significant. This latency may reduce the accuracy of
                                    demand planning.

                                    Modern demand planning solves this problem by basing the forecast on channel demand
                                    rather than the production data. The sales channels provide more accurate, real-time
                                    data. It significantly reduces latency and improves the quality of demand
                                    forecasting.</p>
                                <p>Selecting the wrong priorities: Even when the companies manage to get accurate demand
                                    planning, they often find it hard to prioritize right due to the lack of training in
                                    making good use of forecasts and projections. As a result, companies often
                                    prioritize meeting the immediate requirements. Choosing the right priorities by
                                    following the forecasts in the network design and supply planning models requires
                                    training and practice.</p>
                                <p>Collaborative Planning Forecasting and Replenishment (CPFR): It is the process of
                                    demand planning using the forecasts from the retail network. This model works based
                                    on the assumption that retailers have first-hand information on how fast a product
                                    is moving off the shelf and when the stock needs to be replenished based on that
                                    sales data. The idea is to apply this forecast and conduct demand planning for the
                                    entire supply chain. However, the ability of the retailers to keep track of
                                    accurate, real-time data and interpret it objectively without any bias is
                                    questionable. Their data and their forecasts are unreliable and inaccurate on most
                                    occasions.</p>
                                <h5>Step 2: Choose the right demand planning solution</h5>
                                <p>Demand planning requires multiple business systems such as ERP, order management,
                                    inventory management, etc. As technologies evolve, companies update these systems
                                    with the latest features and functionality, and spend massive amounts of resources
                                    to install and maintain them.</p>
                                <p>However, the resources invested in these supply chain planning applications do not
                                    necessarily translate proportionately into demand planning accuracy. The
                                    satisfaction levels of crucial systems are moderate to low, as given in the table
                                    below.</p>
                                <p class="text-center">
                                    <img src="{{asset('img/blog-img-4.jpg')}}" alt="">
                                </p>
                                <p>The challenge lies in selecting the systems that are best suited for the company.
                                    Companies need to address this issue by conducting a thorough analysis of the data
                                    models of these technologies, compatibility with all the other existing systems, and
                                    suitability for the organization. They need to identify the solutions best suited
                                    for their needs rather than purchasing the best-rated solutions in the market.</p>
                                <h5>Step 3: Execute to perfection</h5>
                                <p>After you put the systems in place, you can become successful only if your
                                    implementation is flawless. You can implement demand planning by following the steps
                                    mentioned below.</p>
                                <h6>Seven best practices to implement demand planning </h6>
                                <p> 1. Establish the base over the years and fine-tune
                                    The focus of your implementation should be on the outcomes rather than achieving
                                    quick results. It takes time to gather a few years of relevant data from various
                                    sources to analyze the patterns and forecast accurately based on historical data.
                                    Betting on premature forecasts can lead to inaccuracy and loss of trust in the
                                    system.</p>
                                <p>So you have to carefully implement demand planning by crafting a well-designed pilot
                                    to identify the market drivers, continuously fine-tuning the systems, and having the
                                    patience to wait until you reach a critical stage where there is an adequate amount
                                    of historical data for producing accurate forecasts.</p>
                                <p>You have to segment the data and the demand flow from various channels. Then you need
                                    to track the streams and refine them until the gap between the predicted and actual
                                    demand closes. For instance, you can segment the demand streams based on the volume
                                    of sales, how fast the products move in the market, how long the products have been
                                    in the market, products on sale, etc., and collect all the data for 2-3 years and
                                    identify the demand patterns to plan the demand for the upcoming year.</p>
                                <p>
                                    2. Adjust the system periodically
                                    There are many processes and systems that need setting up only once. They may yield
                                    consistent results once they are up and running. However, demand planning does not
                                    work that way. You have to clean up the data and refine the demand planning system
                                    with the master data periodically. In addition to this, you must fine-tune the
                                    demand optimization engine and its components periodically.</p>
                                <p>
                                    3. Train your teams
                                    The systems are only as good as the people operating them. So the teams responsible
                                    for maintaining the demand planning system should be trained thoroughly. They should
                                    have a sound leadership that keeps the team sharp and resourceful. The leaders need
                                    to predict employee turnover in this team and implement countermeasures by filling
                                    the positions with suitable employees seamlessly through talent management,
                                    considering there is a scarcity of professionals trained in this discipline.</p>
                                <p>
                                    4. Work on widespread adoption
                                    As mentioned above, accurate demand forecasting requires time and patience. While it
                                    may be possible for you to convince your employees and stakeholders within your
                                    organization, the supply chain is vast, and you need to convince stakeholders from
                                    all levels of the supply chain. Unless you achieve widespread adoption in the supply
                                    chain, it will perpetuate the vicious cycle of lack of end-to-end visibility in the
                                    supply chain and the inability to forecast right because of that.</p>
                                <p>
                                    You will have to work continuously with them and convince them that achieving demand
                                    planning accuracy is a long-term endeavor that requires their participation and
                                    cooperation. Only a change in the collective mindset across the supply chain will
                                    help you succeed in demand planning in the long run. You should also train the
                                    supply chain team to use the system and improve the accuracy of forecasts and demand
                                    planning.</p>
                                <p> 5. Address biases and errors
                                    You need to conduct a forecast Value-Add analysis to address the biases and errors
                                    that lead to inaccuracies. It adds discipline and helps achieve continuous
                                    improvement in the demand planning process.</p>
                                <p> 6. Redesign your processes
                                    Traditionally, demand is forecasted based on what rolls out of the manufacturing
                                    line into the supply chain. You can address demand volatility by reversing the
                                    process, forecasting demand based on what sells in the channels, and deciding what
                                    you manufacture based on that. This model requires a complete redesign of your
                                    manufacturing process.</p>
                                <ul class="list list--check list--reset">

                                    <li class="list__item">You can receive market signals from the channels through the
                                        pilots and validate them.
                                    </li>
                                    <li class="list__item">Using the data from the market based on the channel inputs
                                        and the enterprise clean-up, you can build a data model.
                                    </li>
                                    <li class="list__item">After mapping the market signals, you can build a demand
                                        model for creating forecasts for the channel.
                                    </li>
                                    <li class="list__item">Try to model the “ship-to” locations.</li>
                                    <li class="list__item">Build demand translation capabilities and reduce demand
                                        latency. Use it to sense market variations.
                                    </li>
                                    <li class="list__item">Demand numbers do not have to be specific; you can predict
                                        demand patterns by calculating the probability of demand instead.
                                    </li>
                                </ul>
                                <h5>The way forward</h5>
                                <p>After a decade-long stagnation, it is time to make rapid progress in demand planning,
                                    in line with the requirements of the changing business environment and market
                                    conditions today. You can achieve it only by revisiting your current setup,
                                    identifying the flaws, completely rethinking your demand planning technologies, and
                                    implementing measures for continuous improvement. It is essential to address the
                                    intrinsic issues of bias and errors and put in a lot of effort to build
                                    high-performance teams, train the people, and take your time to refine the processes
                                    and demand sensing models.</p>
                            </div>
                        </div>
                        <div class="row top-20">
                            <div class="col-6">
                                <div class="blog-post__date">02 December 2022</div>
                            </div>
                            <div class="col-6 text-right blog-post__date">SmartChain
                            </div>
                        </div>


                    </div>
                    <div class="col-lg-4 col-xl-3 top-70 top-lg-0">
                        <div class="row">
                            <div class="col-md-6 col-lg-12 bottom-50">
                                <h5 class="blog__title">Categories</h5>
                                <ul class="category-list list--reset">
                                    <li class="category-list__item item--active"><a class="category-list__link"
                                                                                    href="#"><span>Insights</span></a>
                                    </li>
                                    <li class="category-list__item"><a class="category-list__link" href="#"><span>WhitePapers</span></a>
                                    </li>
                                </ul>
                            </div>


                            <div class="col-md-6 col-lg-12">
                                <div class="contact-trigger contact-trigger--style-2"><img class="contact-trigger__img"
                                                                                           src="{{asset('img/contact_background.png')}}"
                                                                                           alt="img"/>
                                    <h4 class="contact-trigger__title">Download the WhitePaper</h4>
                                    <p class="contact-trigger__text">
                                        Click here to download the white paper in PDF format
                                    </p><a class="button button--white"
                                           href="#"><span>Download</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section p-5 bg--lgray">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-lg-7">
                        <div class="heading">
                            <h3 class="heading__title">Get in <span
                                    class="color--green">touch</span></h3>
                        </div>

                        <p class="mt-4">
                            Contact us today to learn more about how the SmartChain team can help your business optimize
                            its supply chain operations.
                        </p>

                    </div>
                    <div class="col-lg-5 text-right">
                        <a class="button button--filled" href="{{route('contact-us')}}"><span>Contact us</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>


    </main>
@endsection

@push('css')
    <style>
        p, h5, h6, ul {
            margin-top: 10px;
        }
    </style>
@endpush
