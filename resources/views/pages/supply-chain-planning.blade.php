@extends('layouts.app')

@section('content')
    <main class="main">
        <!-- section start-->
        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item">
                                <h1 class="hero-block__title">Supply Chain Planning</h1><span class="hero-block__overlay">SmartChain</span>
                                <h5 class="text-white mt-3">Optimize Your Supply Chain Planning with SmartChain's
                                    Solutions</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end-->

        <section class="section bg--lgray">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-xl-5"><img class="bottom-50" src="{{asset('img/site/scp.webp')}}" alt="img">
                        <div class="img-badge"><img class="img-badge__img" src="{{asset('img/badge-img.png')}}" alt="img">
                            <h6 class="img-badge__title bottom-0">
                                From Integrated Business Planning to Inventory Optimization, we have the expertise and tools to optimise supply chain performance
                            </h6>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-6 offset-xl-1">
                        <div class="heading bottom-20"><span class="heading__pre-title">Overview</span>
                            <h3 class="heading__title title-small">Our <span
                                    class="color--green">Supply Chain Planning solutions </span>provide a comprehensive
                                approach to improve your supply chain operations</h3>
                        </div>
                        <p>
                            Supply chain planning is an essential aspect of any business that aims to achieve optimal
                            performance and customer satisfaction. At <strong>SmartChain</strong>, we understand the
                            importance of
                            planning in the supply chain, and we are committed to providing our clients with the best
                            solutions to overcome their supply chain challenges.
                        </p>
                        <ul class="list list--check list--reset">
                            <li class="list__item">
                                We offer a wide range of solutions for all aspects of supply chain planning.
                            </li>
                            <li class="list__item">
                                Our experts have extensive experience in leading supply chain solutions and have
                                successfully implemented them for various industries.
                            </li>
                            <li class="list__item">
                                Our solutions will help you achieve improvements in supply chain performance such as
                                reduced costs, improved efficiency and increased customer satisfaction.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="section positions-archive">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8">
                        <h3 class="positions-archive__title">Our Supply Chain Planning Solutions</h3>
                        <div class="accordion accordion--primary">
                            <div class="accordion__title-block">
                                <h5 class="accordion__title">Integrated Business Planning / S&OP </h5><span
                                    class="accordion__close"></span>
                            </div>
                            <div class="accordion__text-block" style="display: none;">
                                <h6>Optimize Your Business with Integrated Business Planning and S&OP</h6>
                                <p>
                                    Our Integrated Business Planning and Sales & Operations Planning (S&OP) services
                                    provide a comprehensive assessment of your current business operations, identify
                                    areas of improvement, and develop a plan for achieving your business goals..</p>
                                <ul class="list list--check list--reset">
                                    <li class="list__item">Improve forecast accuracy</li>
                                    <li class="list__item">Enhance collaboration across functions</li>
                                    <li class="list__item">Align sales, operations and finance</li>
                                    <li class="list__item">Increase visibility and control</li>
                                </ul>
                            </div>
                        </div>
                        <div class="accordion accordion--primary">
                            <div class="accordion__title-block">
                                <h5 class="accordion__title">Demand Planning</h5><span class="accordion__close"></span>
                            </div>
                            <div class="accordion__text-block" style="display: none;">
                                <h6>Improve Your Demand Forecasting with SmartChain's Demand Planning Services</h6>
                                <p>
                                    Our Demand Planning services provide a comprehensive assessment of your current
                                    demand forecasting process, identify areas of improvement, and develop a plan for
                                    achieving your business goals.
                                </p>
                                <ul class="list list--check list--reset">
                                    <li class="list__item">Improve demand forecasting accuracy</li>
                                    <li class="list__item">Enhance collaboration across functions</li>
                                    <li class="list__item">Align sales, marketing and operations</li>
                                    <li class="list__item">Increase visibility and control</li>
                                </ul>
                            </div>
                        </div>
                        <div class="accordion accordion--primary">
                            <div class="accordion__title-block">
                                <h5 class="accordion__title">Supply Planning </h5><span class="accordion__close"></span>
                            </div>
                            <div class="accordion__text-block">
                                <h6>Optimize Your Supply Chain with SmartChain's Supply Planning Services</h6>
                                <p>
                                    Our Supply Planning services provide a comprehensive assessment of your current
                                    supply chain operations, identify areas of improvement, and develop a plan for
                                    achieving your business goals.
                                </p>
                                <ul class="list list--check list--reset">
                                    <li class="list__item">Improve supply forecasting accuracy</li>
                                    <li class="list__item">Enhance collaboration across functions</li>
                                    <li class="list__item">Align supply, operations and finance</li>
                                    <li class="list__item">Increase visibility and control</li>
                                </ul>
                            </div>
                        </div>
                        <div class="accordion accordion--primary">
                            <div class="accordion__title-block">
                                <h5 class="accordion__title">Inventory Optimization</h5><span
                                    class="accordion__close"></span>
                            </div>
                            <div class="accordion__text-block">
                                <p>Our Inventory Optimization services provide a comprehensive assessment of your
                                    current inventory management process, identify areas of improvement, and develop a
                                    plan for achieving your business goals. </p>
                                <ul class="list list--check list--reset">
                                    <li class="list__item">Improve inventory turnover</li>
                                    <li class="list__item">Reduce inventory carrying costs</li>
                                    <li class="list__item">Increase service levels</li>
                                    <li class="list__item">Improve collaboration across functions</li>
                                </ul>
                            </div>
                        </div>
                        <div class="accordion accordion--primary">
                            <div class="accordion__title-block">
                                <h5 class="accordion__title">Constraints and Capacity planning</h5><span
                                    class="accordion__close"></span>
                            </div>
                            <div class="accordion__text-block">
                                <h6>Optimize Your Capacity and Overcome Constraints with SmartChain's Capacity Planning
                                    Services</h6>
                                <p>Our Constraints and Capacity Planning services provide a comprehensive assessment of
                                    your current capacity and constraints, identify areas of improvement, and develop a
                                    plan for achieving your business goals.</p>
                                <ul class="list list--check list--reset">
                                    <li class="list__item">Improve capacity utilization</li>
                                    <li class="list__item">Increase production efficiency</li>
                                    <li class="list__item">Reduce lead times</li>
                                    <li class="list__item">Improve collaboration across functions</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xl-3 offset-xl-1">
                        <div class="contact-trigger contact-trigger--style-2">
                            <img class="contact-trigger__img" src="{{asset('img/contact_background.png')}}" alt="img">
                            <h4 class="contact-trigger__title">Know more about SmartChain</h4>
                            <p class="contact-trigger__text">
                                Contact us today to learn more about how the SmartChain team can help your business optimize
                                its supply chain operations.
                            </p><a class="button button--white" href="{{route('contact-us')}}"><span>Get in Touch</span>
                                <ion-icon name="arrow-forward"></ion-icon>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section bg--lgray features-front_6">
            <div class="container">
                <div class="row bottom-50">
                    <div class="col-12">
                        <div class="heading"><span class="heading__pre-title">Features</span>
                            <h3 class="heading__title">How our <span class="color--green">Supply chain planning</span> <br>
                                can impact your business</h3>
                            <span class="heading__layout layout--lgray">Features</span>
                        </div>
                    </div>
                </div>
                <div class="row offset-50">
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="info-item">
                            <div class="info-item__img">
                                <ion-icon name="bar-chart" role="img" class="md hydrated" aria-label="bar chart"></ion-icon>
                            </div>
                            <div class="info-item__details">
                                <strong>Improve forecasting accuracy</strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="info-item">
                            <div class="info-item__img">
                                <ion-icon name="checkmark-circle-outline" role="img" class="md hydrated" aria-label="checkmark circle outline"></ion-icon>
                            </div>
                            <div class="info-item__details">
                                <strong>Reduce lead times</strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="info-item">
                            <div class="info-item__img">
                                <ion-icon name="cash-outline" role="img" class="md hydrated" aria-label="cash outline"></ion-icon>
                            </div>
                            <div class="info-item__details">
                                <strong>Increase inventory turnover</strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="info-item">
                            <div class="info-item__img">
                                <ion-icon name="checkmark-circle-outline" role="img" class="md hydrated" aria-label="checkmark circle outline"></ion-icon>
                            </div>
                            <div class="info-item__details">
                                <strong>Improve capacity utilization</strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="info-item">
                            <div class="info-item__img">
                                <ion-icon name="caret-up-circle-outline" role="img" class="md hydrated" aria-label="caret up circle outline"></ion-icon>
                            </div>
                            <div class="info-item__details">
                                <strong>Increase production efficiency</strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="info-item">
                            <div class="info-item__img">
                                <ion-icon name="caret-up-circle-outline" role="img" class="md hydrated" aria-label="caret up circle outline"></ion-icon>
                            </div>
                            <div class="info-item__details">
                                <strong>Improve collaboration across functions</strong>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <div class="cta-block cta-block--style-2"><img class="img--bg" src="{{asset('img/site/cta.webp')}}" alt="bg"/>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8">
                        <div class="heading heading--white">
                            <h5 class="heading__title title-small">
                                Contact us today to learn more about how the SmartChain team can help your business optimize
                                its supply chain operations.
                            </h5>
                        </div>
                    </div>
                    <div class="col-lg-4 text-lg-right"><a class="button button--white"
                                                           href="{{route('contact-us')}}"><span>Get in touch</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @include('includes.insights')

    </main>
@endsection
