@extends('layouts.app')

@section('content')
    <main class="main">

        <!-- section start-->
        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/hero1.jpg')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">Implementation Services</h1>
                                <h5 class="text-white mt-3">
                                    Operations and Technology
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end-->
        <section class="section service-details">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-xl-9">
                        <h5 class="service-details__title mb-4">Implementation Services</h5>
                        <img class="service-details__img" src="{{asset('img/site/imp.webp')}}" alt="img">

                        <h6>
                            &quot; Our implementation services provide comprehensive support for companies looking to
                            implement
                            or upgrade their supply chain operations and technologies&quot;
                        </h6>
                        <p>
                            Our approach is guided by project
                            management best practices, governance frameworks, and Agile methodologies such as Hybrid
                            Agile
                            and Scrum teams. This ensures that our clients&#39; projects are executed smoothly and
                            effectively, with
                            minimal disruptions to their operations. Our services also include support services to
                            ensure that our
                            clients receive the assistance they need to ensure successful implementation and adoption of
                            new
                            technologies.
                        </p>
                        <div class="row top-20 offset-30" id="services-cards">
                            <div class="col-sm-6 col-xl-4">
                                <a href="#poc" class="service-link">
                                    <div class="service-benefits small">
                                        <ion-icon name="star"></ion-icon>
                                        <h6 class="service-benefits__title">POC/Rollouts/Upgrades</h6>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-xl-4">
                                <a href="#development" class="service-link">
                                    <div class="service-benefits">
                                        <ion-icon name="star"></ion-icon>
                                        <h6 class="service-benefits__title">Development</h6>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-xl-4">
                                <a href="#integration" class="service-link">
                                    <div class="service-benefits">
                                        <ion-icon name="star"></ion-icon>
                                        <h6 class="service-benefits__title">Integration</h6>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-xl-4">
                                <a href="#testing" class="service-link">
                                    <div class="service-benefits">
                                        <ion-icon name="star"></ion-icon>
                                        <h6 class="service-benefits__title">Testing</h6>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-xl-4">
                                <a href="#training" class="service-link">
                                    <div class="service-benefits">
                                        <ion-icon name="star"></ion-icon>
                                        <h6 class="service-benefits__title">Training</h6>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-xl-4">
                                <a href="#support" class="service-link">
                                    <div class="service-benefits">
                                        <ion-icon name="star"></ion-icon>
                                        <h6 class="service-benefits__title">Support Services</h6>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xl-3 top-50 top-lg-0">
                        <div class="row">
                            <div class="col-md-6 col-lg-12 bottom-50">
                                <h5 class="blog__title">Our Services</h5>
                                <ul class="category-list list--reset">
                                    <li class="category-list__item"><a class="category-list__link"
                                                                       href="{{route('services.business-advisory-service')}}"><span>Business Consulting Service</span></a>
                                    </li>
                                    <li class="category-list__item item--active"><a class="category-list__link"
                                                                                    href="javascript:"><span>Implementation Service</span></a>
                                    </li>
                                    <li class="category-list__item"><a class="category-list__link"
                                                                       href="{{route('services.technology-advisory-service')}}"><span>Technology Consulting Service</span></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-lg-12">
                                <div class="contact-trigger contact-trigger--style-2">
                                    <img class="contact-trigger__img" src="{{asset('img/contact_background.png')}}"
                                         alt="img">
                                    <h4 class="contact-trigger__title">Know more about SmartChain</h4>
                                    <p class="contact-trigger__text">
                                        Get in touch with us to know more about or company and how we operate...
                                    </p><a class="button button--white" href="{{route('contact-us')}}"><span>Know more about Us</span>
                                        <ion-icon name="arrow-forward"></ion-icon>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="poc" class="section bg--lgray">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-5">
                        <h4 class="bottom-20">

                            POC (Proof of Concept) Service
                        </h4>
                        <p>The POC (Proof of Concept) Service offered by SmartChain is designed to help
                            organizations validate the feasibility and viability of our logistics technology
                            solutions.</p>

                    </div>
                    <div class="col-lg-6 col-xl-6 offset-xl-1 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits</h6>
                        <ul class="list list--check list--reset">
                            <li class="list__item">A team of experts will work with you to design and implement a
                                prototype of your proposed
                                solution.
                            </li>
                            <li class="list__item">Detailed report on the potential benefits, limitations, and any
                                issues that need to be
                                addressed.
                            </li>
                            <li class="list__item">Helps you make an informed decision about whether to invest in a
                                full-scale implementation
                                of our technology.
                            </li>
                        </ul>
                    </div>
                    <a class="button--filled mt-3 ml-3 p-2 px-3 text-white" href="#services-cards">
                        <ion-icon size="large" name="arrow-up"></ion-icon>
                    </a>
                </div>
            </div>
        </section>

        <section id="" class="section">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-5">

                        <h4 class="bottom-20">Roll Out Service
                        </h4>
                        <p>The Roll Out Service offered by SmartChain is aimed at helping organizations
                            successfully implement our logistics technology solutions across their entire supply
                            chain.</p>

                    </div>
                    <div class="col-lg-6 col-xl-6 offset-xl-1 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits</h6>
                        <ul class="list list--check list--reset">
                            <li class="list__item">A team of experts will work with you to plan and coordinate the roll
                                out.
                            </li>
                            <li class="list__item">Ensures that the new technology is integrated seamlessly into your
                                existing systems and
                                processes.
                            </li>
                            <li class="list__item">Ongoing support and training to ensure a successful adoption of our
                                technology by your
                                employees.
                            </li>
                        </ul>
                    </div>
                    <a class="button--filled mt-3 ml-3 p-2 px-3 text-white" href="#services-cards">
                        <ion-icon size="large" name="arrow-up"></ion-icon>
                    </a>
                </div>
            </div>
        </section>

        <section id="" class="section bg--lgray">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-5">

                        <h4 class="bottom-20">Upgrade Service
                        </h4>
                        <p>
                            SmartChain is dedicated to providing the best logistics technology solutions on the
                            market, and our Upgrade Service helps organizations stay current with the latest
                            advancements in
                            our field.
                        </p>

                    </div>
                    <div class="col-lg-6 col-xl-6 offset-xl-1 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits</h6>
                        <ul class="list list--check list--reset">
                            <li class="list__item">A team of experts will work with you to plan and implement upgrades
                                to your existing
                                solutions.
                            </li>
                            <li class="list__item">Ensures that your solutions are up-to-date and secure.</li>
                            <li class="list__item">Helps you get the most out of your SmartChain technology investment,
                                whether it&#39;s fixing
                                bugs, improving performance, or adding new features.
                            </li>
                        </ul>
                    </div>
                    <a class="button--filled mt-3 ml-3 p-2 px-3 text-white" href="#services-cards">
                        <ion-icon size="large" name="arrow-up"></ion-icon>
                    </a>
                </div>
            </div>
        </section>

        <section id="development" class="section">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-5">

                        <h4 class="bottom-20">Development
                        </h4>
                        <p>
                            At SmartChain, we understand that every business has unique requirements and challenges when
                            it
                            comes to logistics technology. That&#39;s why our SmartChain Developments service is
                            designed to
                            provide custom development services that meet the specific needs of your business.
                        </p>
                        <p>Our development process begins with a thorough understanding of your business and its
                            specific
                            needs. Our team of experts will work closely with you to gain insights into your current
                            processes
                            and identify areas where our technology solutions can help optimize and streamline your
                            operations.</p>
                        <p>Next, our team will design and develop a tailored solution that meets your unique
                            requirements,
                            incorporating the latest advancements in logistics technology to ensure the best possible
                            outcome.
                            Throughout the development process, we will work closely with you to ensure that your
                            solution is
                            delivered on time, within budget, and meets all your expectations.</p>

                    </div>
                    <div class="col-lg-6 col-xl-6 offset-xl-1 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits</h6>
                        <ul class="list list--check list--reset">
                            <li class="list__item">Custom development services tailored to your specific business
                                needs
                            </li>
                            <li class="list__item">Solutions designed to meet the unique requirements of your business
                            </li>
                            <li class="list__item">A bespoke solution that is perfectly tailored to your business
                                needs
                            </li>
                            <li class="list__item">Thorough understanding of your business and its specific needs</li>
                            <li class="list__item">Expert design and development services from our team of logistics
                                technology experts
                            </li>
                            <li class="list__item">Ongoing support and collaboration throughout the development process
                                to ensure the best
                                possible outcome.
                            </li>
                        </ul>
                    </div>
                    <a class="button--filled mt-3 ml-3 p-2 px-3 text-white" href="#services-cards">
                        <ion-icon size="large" name="arrow-up"></ion-icon>
                    </a>
                </div>
            </div>
        </section>

        <section id="integration" class="section bg--lgray">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-5">

                        <h4 class="bottom-20">Integration
                        </h4>
                        <p>At SmartChain, we understand the importance of seamlessly integrating logistics technology
                            solutions into your existing systems and processes. That&#39;s why we offer SmartChain
                            Integration
                            services to help you get the most out of your investment.</p>
                        <p>Our team of experts will work with you to design and implement an integration plan that meets
                            your
                            specific needs. We will ensure that our solutions are integrated smoothly and efficiently
                            into your
                            existing systems, minimizing downtime and ensuring a seamless user experience.</p>
                        <p>Whether you need to integrate our solutions with your existing logistics management software,
                            enterprise resource planning (ERP) systems, or any other business-critical applications, our
                            SmartChain Integration services have you covered.</p>

                    </div>
                    <div class="col-lg-6 col-xl-6 offset-xl-1 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits</h6>
                        <ul class="list list--check list--reset">
                            <li class="list__item">Integration services tailored to your specific needs</li>
                            <li class="list__item">Seamless integration of logistics technology solutions into your
                                existing systems and
                                processes
                            </li>
                            <li class="list__item">Minimized downtime during integration</li>
                            <li class="list__item">A smooth and seamless user experience for your employees</li>
                            <li class="list__item">Expert design and implementation of integration plans by our team of
                                logistics technology
                                experts
                            </li>
                            <li class="list__item">Ongoing support and collaboration to ensure the best possible
                                outcome.
                            </li>
                        </ul>
                    </div>
                    <a class="button--filled mt-3 ml-3 p-2 px-3 text-white" href="#services-cards">
                        <ion-icon size="large" name="arrow-up"></ion-icon>
                    </a>
                </div>
            </div>
        </section>

        <section id="testing" class="section">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-5">

                        <h4 class="bottom-20">Testing
                        </h4>
                        <p>We understand that ensuring the quality and reliability of your logistics technology
                            solutions is
                            critical to the success of your business. That&#39;s why we offer SmartChain Testing
                            services to help you
                            get the most out of your investment.</p>
                        <p>We work with you to design and implement a comprehensive testing plan that meets your
                            specific
                            needs. Whether you need to test the functionality and performance of your logistics
                            management
                            software, ensure the security and stability of your systems, or validate the compliance of
                            your
                            processes with industry standards, we have you covered.</p>
                        <p>Our SmartChain Testing services include both manual and automated testing methods to ensure
                            that
                            your solutions are thoroughly tested, validated, and optimized for performance, security,
                            and
                            compliance. Our testing automation services help you to minimize manual testing effort,
                            reduce the
                            risk of human error, and increase test coverage and efficiency.</p>

                    </div>
                    <div class="col-lg-6 col-xl-6 offset-xl-1 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits</h6>
                        <ul class="list list--check list--reset">
                            <li class="list__item">Testing services tailored to your specific needs</li>
                            <li class="list__item">Comprehensive testing plans designed and implemented by our team of
                                testing experts
                            </li>
                            <li class="list__item">Thorough validation of the functionality, performance, security, and
                                compliance of your
                                logistics technology solutions
                            </li>
                            <li class="list__item">Expert optimization of your systems for performance, security, and
                                compliance
                            </li>
                            <li class="list__item">Minimized manual testing effort through testing automation</li>
                            <li class="list__item">Reduced risk of human error through testing automation</li>
                            <li class="list__item">Increased test coverage and efficiency through testing automation
                            </li>
                            <li class="list__item">Ongoing support and collaboration to ensure the best possible
                                outcome.
                            </li>
                        </ul>
                    </div>
                    <a class="button--filled mt-3 ml-3 p-2 px-3 text-white" href="#services-cards">
                        <ion-icon size="large" name="arrow-up"></ion-icon>
                    </a>
                </div>
            </div>
        </section>

        <section id="training" class="section bg--lgray">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-12">

                        <h4 class="bottom-20">Training
                        </h4>
                        <p>At SmartChain, we understand the importance of effective training in ensuring the successful
                            adoption and use of logistics technology solutions. Our team of training experts, along with
                            domain
                            experts and product experts, work together to build customised training programs to meet
                            your
                            specific needs.</p>
                        <p>TTT (Technical Training for IT Professionals): Our TTT services are designed to provide
                            technical
                            training for IT professionals, developers, or other technical staff. Our team of experts
                            will work with
                            you to deliver a comprehensive training program that covers all the technical aspects of
                            your
                            logistics technology solution.</p>
                        <p>End User Training: Our end user training services are designed to help non-technical users
                            get the
                            most out of your logistics technology solution. Our training experts will work with you to
                            design a
                            training program that meets the specific needs of your end users, so that they can
                            effectively use
                            and benefit from the technology.</p>

                    </div>
                    <div class="col-lg-12 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits</h6>
                        <div class="row">
                            <div class="col-md-6">
                                <strong>TTT (Technical Training for IT Professionals)</strong>
                                <ul class="list list--check list--reset">
                                    <li class="list__item">Customised training program built by our team of training
                                        experts,
                                        domain experts, and
                                        product experts
                                    </li>
                                    <li class="list__item">Comprehensive technical training for IT professionals,
                                        developers, or
                                        other technical staff
                                    </li>
                                    <li class="list__item">Increased technical knowledge and expertise</li>
                                    <li class="list__item">Reduced downtime and support costs</li>
                                    <li class="list__item">Ensured long-term success of logistics technology projects
                                    </li>
                                    <li class="list__item">Ongoing support and collaboration to ensure the best possible
                                        outcome.
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <strong>End User Training</strong>
                                <ul class="list list--check list--reset">
                                    <li class="list__item">Customised training program built by our team of training
                                        experts,
                                        domain experts, and
                                        product experts
                                    </li>
                                    <li class="list__item">Comprehensive end user training for non-technical users</li>
                                    <li class="list__item">Increased user adoption and satisfaction</li>
                                    <li class="list__item">Increased understanding and effective use of the technology
                                    </li>
                                    <li class="list__item">Reduced downtime and support costs</li>
                                    <li class="list__item">Ensured long-term success of logistics technology projects
                                    </li>
                                    <li class="list__item">Ongoing support and collaboration to ensure the best possible
                                        outcome.
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <a class="button--filled mt-3 ml-3 p-2 px-3 text-white" href="#services-cards">
                        <ion-icon size="large" name="arrow-up"></ion-icon>
                    </a>
                </div>
            </div>
        </section>

        <section id="support" class="section">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-12">

                        <h4 class="bottom-20">Support Services
                        </h4>
                        <p>SmartChain provides comprehensive support services to ensure the smooth deployment,
                            operation,
                            and maintenance of its technology solutions. Our team of experts works with customers to
                            resolve
                            any issues and ensure their technology is functioning optimally.</p>

                    </div>
                    <div class="col-lg-12 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits</h6>
                        <div class="row">
                            <div class="col-md-6">
                                <p>Technical support: SmartChain offers technical support to assist with installation,
                                    configuration, and
                                    maintenance of its technology solutions, as well as troubleshooting and resolving
                                    technical
                                    issues
                                    that may arise.</p>
                            </div>
                            <div class="col-md-6">
                                <p>Compliance support: SmartChain uses a handshake approach to ensure its technology
                                    solutions
                                    are
                                    in compliance with relevant industry standards and regulations. This service helps
                                    customers
                                    understand and meet these standards, and resolves any compliance-related issues.</p>
                            </div>
                            <div class="col-md-6">
                                <p>Root cause analysis: SmartChain&#39;s support services include root cause analysis to
                                    solve
                                    the
                                    underlying cause of any issues that may arise, to minimize the risk of similar
                                    issues
                                    occurring in the
                                    future.</p>
                            </div>
                            <div class="col-md-6">
                                <p>User training: To ensure that customers fully understand and effectively use the
                                    technology
                                    solution,
                                    SmartChain provides user training and documentation resources, including end-user
                                    training
                                    and
                                    training for IT administrators and support staff.</p>
                            </div>
                            <div class="col-md-6">
                                <p>Ongoing maintenance and upgrades: Regular software updates and upgrades are important
                                    to
                                    ensure that the technology solution remains up-to-date and continues to function
                                    optimally.
                                    SmartChain provides maintenance and repair services in the event of a hardware or
                                    software
                                    failure.</p>
                            </div>
                            <div class="col-md-6">
                                <p>Overall, SmartChain&#39;s support services play a crucial role in ensuring the smooth
                                    deployment,
                                    operation, and maintenance of its technology solutions, providing customers with the
                                    resources and
                                    assistance they need to resolve any issues and ensure their technology is
                                    functioning
                                    optimally.</p>
                            </div>
                        </div>
                    </div>
                    <a class="button--filled mt-3 ml-3 p-2 px-3 text-white" href="#services-cards">
                        <ion-icon size="large" name="arrow-up"></ion-icon>
                    </a>
                </div>
            </div>
        </section>


        <div class="cta-block cta-block--style-2"><img class="img--bg" src="{{asset('img/site/cta.webp')}}" alt="bg"/>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8">
                        <div class="heading heading--white">
                            <h5 class="heading__title title-small">
                                Contact us today to learn more about how the SmartChain team can help your business
                                optimize
                                its supply chain operations.
                            </h5>
                        </div>
                    </div>
                    <div class="col-lg-4 text-lg-right"><a class="button button--white"
                                                           href="{{route('contact-us')}}"><span>Get in touch</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @include('includes.insights')


    </main>
@endsection
