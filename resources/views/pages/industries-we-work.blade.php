@extends('layouts.app')

@section('content')
    <main class="main">

        <!-- section start-->
        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">Industries we work</h1>
                                <h5 class="text-white mt-3">

                                </h5>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- section end-->

        <section class="section">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-xl-8 top-50 top-xl-0">
                        <div class="heading bottom-20"><span class="heading__pre-title">Industry</span>
                            <h3 class="heading__title">Automotive Industry</h3>
                        </div>
                        <p class="bottom-0">
                            Logistics technology plays a crucial role in the automotive industry, as
                            it helps companies manage the supply chain effectively, reducing lead
                            times and ensuring timely delivery of parts and vehicles to customers.
                            With the increasing innovation and competition in the industry, having a
                            robust logistics system is crucial for companies to meet the demands of
                            their customers.
                        </p>

                        <div class="container pt-5">
                            <div class="accordion accordion--primary">
                                <div class="accordion__title-block">
                                    <h5 class="accordion__title">Key Challenges in Logistics Function in this
                                        Industry </h5><span class="accordion__close"></span>
                                </div>
                                <div class="accordion__text-block" style="display: none;">
                                    <ul class="list list--check list--reset">
                                        <li class="list__item">Changing supply sources and tariffs</li>
                                        <li class="list__item">Lack of supply chain visibility and agility</li>
                                        <li class="list__item">Longer lead times</li>
                                        <li class="list__item">Lost customers</li>
                                        <li class="list__item">Severe part shortages from suppliers, leading to global
                                            vehicle demand constraints
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="accordion accordion--primary">
                                <div class="accordion__title-block">
                                    <h5 class="accordion__title">How SmartChain Services and Solutions can help</h5>
                                    <span class="accordion__close"></span>
                                </div>
                                <div class="accordion__text-block" style="display: none;">
                                    <p>
                                        Our implementation and
                                        logistics solutions can help companies in the automotive industry overcome
                                        these challenges and improve their logistics operations. Our POC, roll-out, and
                                        upgrade services can help companies validate and implement our
                                        logistics technology solutions effectively. Our development and
                                        customization services can help companies adapt our solutions to meet
                                        their specific needs. Our integration and testing services ensure that all
                                        systems are working seamlessly together. Our training and support services
                                        provide companies with the knowledge and resources they need to get the
                                        most out of their SmartChain investment. Our solutions can help companies
                                        improve supply chain visibility, reduce lead times, increase efficiency,
                                        and make informed decisions about their logistics operations.
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 text-center text-xl-left">
                        <img class="truck-img"
                             src="{{asset('img/icons/Asset 3.svg')}}"
                             alt="img">
                    </div>
                </div>
            </div>
        </section>

        <section class="section bg--lgray">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-xl-8 top-50 top-xl-0 order-lg-last">
                        <div class="heading bottom-20"><span class="heading__pre-title">Industry</span>
                            <h3 class="heading__title">Consumer Packaged Goods (CPG)</h3>
                        </div>
                        <p class="bottom-0">
                            The Consumer Packaged Goods (CPG) industry is facing new and complex
                            challenges, such as evolving customer behavior, increasing demand for
                            sustainable and competitively priced products, and the need for multiple
                            delivery options. In order to remain competitive and meet the changing
                            needs of customers, logistics technology is becoming increasingly
                            important for CPG companies.
                        </p>

                        <div class="container pt-5">
                            <div class="accordion accordion--primary">
                                <div class="accordion__title-block">
                                    <h5 class="accordion__title">Key Challenges in Logistics Function in this
                                        Industry </h5><span class="accordion__close"></span>
                                </div>
                                <div class="accordion__text-block" style="display: none;">
                                    <p>The CPG industry faces a number of challenges in its logistics function,
                                        including increased supply chain costs, the need for greater supply chain
                                        visibility and agility, and the requirement to meet customer expectations
                                        for product sustainability and competitive pricing. Additionally, CPG
                                        companies must also navigate a rapidly evolving landscape, where customer
                                        behavior is driving new product requirements and delivery options.</p>
                                </div>
                            </div>
                            <div class="accordion accordion--primary">
                                <div class="accordion__title-block">
                                    <h5 class="accordion__title">How SmartChain Services and Solutions can help</h5>
                                    <span class="accordion__close"></span>
                                </div>
                                <div class="accordion__text-block" style="display: none;">
                                    <p>
                                        SmartChain&#39;s services and solutions can help CPG companies to overcome
                                        these challenges by providing advanced logistics technology that can
                                        improve supply chain visibility, increase agility, and reduce costs. Our
                                        solutions are designed to help CPG companies streamline their operations,
                                        stay ahead of the curve in a rapidly changing market, and meet the
                                        evolving needs of their customers. Additionally, our team of experts can
                                        provide training, support, and ongoing maintenance to ensure that CPG
                                        companies get the most out of their technology investment.
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 text-center text-xl-left">
                        <img class="truck-img"
                             src="{{asset('img/icons/Asset 6.svg')}}"
                             alt="img">
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-xl-8 top-50 top-xl-0">
                        <div class="heading bottom-20"><span class="heading__pre-title">Industry</span>
                            <h3 class="heading__title">High Tech Industry</h3>
                        </div>
                        <p class="bottom-0">
                            Logistics technology plays a critical role in the high tech industry,
                            where the speed and complexity of supply chains have increased
                            significantly in recent years. Logistics technology helps to ensure that
                            goods are delivered efficiently and effectively to customers, while also
                            reducing costs and improving service levels.
                        </p>

                        <div class="container pt-5">
                            <div class="accordion accordion--primary">
                                <div class="accordion__title-block">
                                    <h5 class="accordion__title">Key Challenges in Logistics Function in this
                                        Industry </h5><span class="accordion__close"></span>
                                </div>
                                <div class="accordion__text-block" style="display: none;">
                                    <p>The high tech industry faces a number of challenges in its logistics
                                        function, including the global nature of supply chains, the complexity of
                                        distribution channels, market competition, high customer service
                                        expectations, and increased risk, including trade/tariff uncertainties.</p>
                                </div>
                            </div>
                            <div class="accordion accordion--primary">
                                <div class="accordion__title-block">
                                    <h5 class="accordion__title">How SmartChain Services and Solutions can help</h5>
                                    <span class="accordion__close"></span>
                                </div>
                                <div class="accordion__text-block" style="display: none;">
                                    <p>
                                        SmartChain&#39;s services and solutions can help the high tech industry
                                        address these challenges by providing the technology, expertise, and
                                        support needed to manage complex and dynamic supply chains. Our POC (Proof
                                        of Concept) service helps organizations validate the feasibility and
                                        viability of our logistics technology solutions, while our roll out
                                        service helps with the successful implementation of our technology across
                                        the entire supply chain. Our upgrade service ensures that organizations
                                        stay current with the latest advancements in logistics technology, and get
                                        the most out of their investment. By working with SmartChain,
                                        organizations in the high tech industry can improve their supply chain
                                        performance and meet the demands of the market.
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 text-center text-xl-left">
                        <img class="truck-img"
                             src="{{asset('img/icons/Asset 13.svg')}}"
                             alt="img">
                    </div>
                </div>
            </div>
        </section>

        <section class="section bg--lgray">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-xl-8 top-50 top-xl-0 order-lg-last">
                        <div class="heading bottom-20"><span class="heading__pre-title">Industry</span>
                            <h3 class="heading__title">Life Sciences</h3>
                        </div>
                        <p class="bottom-0">
                            The life sciences industry is critical as it provides essential
                            medications and services that affect people&#39;s health and well-being. With
                            the increasing supply chain complexity and volatility, logistics
                            technology plays a crucial role in ensuring the efficient and reliable
                            delivery of products and services to customers.
                        </p>

                        <div class="container pt-5">
                            <div class="accordion accordion--primary">
                                <div class="accordion__title-block">
                                    <h5 class="accordion__title">Key Challenges in Logistics Function in this
                                        Industry </h5><span class="accordion__close"></span>
                                </div>
                                <div class="accordion__text-block" style="display: none;">
                                    <p>The life sciences industry faces several challenges in the logistics
                                        function, including increased supply chain complexity, increased risk due
                                        to volatility, and the need to work with multiple business partners. These
                                        challenges can lead to disruptions in the supply chain, which can result
                                        in adverse effects on key metrics, such as longer lead times, higher
                                        costs, and decreased customer satisfaction.</p>
                                </div>
                            </div>
                            <div class="accordion accordion--primary">
                                <div class="accordion__title-block">
                                    <h5 class="accordion__title">How SmartChain Services and Solutions can help</h5>
                                    <span class="accordion__close"></span>
                                </div>
                                <div class="accordion__text-block" style="display: none;">
                                    <p>
                                        SmartChain services and solutions can help the life sciences industry
                                        overcome these challenges by providing real-time visibility and control
                                        over the entire supply chain. This includes real-time monitoring of
                                        inventory levels, order status, delivery performance, and supplier
                                        performance. Additionally, SmartChain solutions can help optimize the
                                        supply chain by identifying inefficiencies and bottlenecks, and by
                                        implementing best practices and advanced technologies. By leveraging the
                                        power of SmartChain solutions, the life sciences industry can ensure
                                        reliable and efficient delivery of products and services, and ultimately
                                        improve the health and well-being of their customers.
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 text-center text-xl-left">
                        <img class="truck-img"
                             src="{{asset('img/icons/Asset 2.svg')}}"
                             alt="img">
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-xl-8 top-50 top-xl-0">
                        <div class="heading bottom-20"><span class="heading__pre-title">Industry</span>
                            <h3 class="heading__title">Manufacturing</h3>
                        </div>
                        <p class="bottom-0">
                            The Manufacturing industry faces unique challenges in the logistics function, such as
                            managing complex supply chains, ensuring product quality, and reducing costs.The
                            manufacturing industry is critical for producing the products that drive our economy and
                            improve our daily lives. With the increasing demand for high-quality products and on-time
                            delivery, logistics technology plays a crucial role in ensuring the efficient and reliable
                            delivery of products and services to customers.
                        </p>

                        <div class="container pt-5">
                            <div class="accordion accordion--primary">
                                <div class="accordion__title-block">
                                    <h5 class="accordion__title">Key Challenges in Logistics Function in this
                                        Industry </h5><span class="accordion__close"></span>
                                </div>
                                <div class="accordion__text-block" style="display: none;">
                                    <p>The manufacturing industry faces several challenges in the logistics function,
                                        including increased demand for high-quality products and on-time delivery,
                                        increased competition, and the need to work with multiple business partners.
                                        These challenges can lead to disruptions in the supply chain, which can result
                                        in adverse effects on key metrics, such as longer lead times, higher costs, and
                                        decreased customer satisfaction.</p>
                                </div>
                            </div>
                            <div class="accordion accordion--primary">
                                <div class="accordion__title-block">
                                    <h5 class="accordion__title">How SmartChain Services and Solutions can help</h5>
                                    <span class="accordion__close"></span>
                                </div>
                                <div class="accordion__text-block" style="display: none;">
                                    <p>
                                        SmartChain services and solutions can help the manufacturing industry overcome
                                        these challenges by providing real-time visibility and control over the entire
                                        supply chain. This includes real-time monitoring of inventory levels, order
                                        status, delivery performance, and supplier performance. Additionally, SmartChain
                                        solutions can help optimize the supply chain by identifying inefficiencies and
                                        bottlenecks, and by implementing best practices and advanced technologies. By
                                        leveraging the power of SmartChain solutions, the manufacturing industry can
                                        ensure reliable and efficient delivery of products and services, and ultimately
                                        improve their competitiveness in the market.
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 text-center text-xl-left">
                        <img class="truck-img"
                             src="{{asset('img/icons/Asset 1.svg')}}"
                             alt="img">
                    </div>
                </div>
            </div>
        </section>

        <section class="section bg--lgray">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-xl-8 top-50 top-xl-0 order-lg-last">
                        <div class="heading bottom-20"><span class="heading__pre-title">Industry</span>
                            <h3 class="heading__title">Third Party Logistics (3PL)</h3>
                        </div>
                        <p class="bottom-0">
                            The Third Party Logistics (3PL) industry plays a critical role in supporting the supply
                            chain operations of organizations across various industries. With the increasing demand for
                            efficient and cost-effective logistics solutions, the 3PL industry is becoming increasingly
                            important.
                        </p>

                        <div class="container pt-5">
                            <div class="accordion accordion--primary">
                                <div class="accordion__title-block">
                                    <h5 class="accordion__title">Key Challenges in Logistics Function in this
                                        Industry </h5><span class="accordion__close"></span>
                                </div>
                                <div class="accordion__text-block" style="display: none;">
                                    <p>
                                        The 3PL industry faces several challenges in the logistics function, including
                                        increased demand for cost-effective solutions, increased competition, and the
                                        need to work with multiple business partners. These challenges can lead to
                                        disruptions in the supply chain, which can result in adverse effects on key
                                        metrics, such as longer lead times, higher costs, and decreased customer
                                        satisfaction.

                                    </p>
                                </div>
                            </div>
                            <div class="accordion accordion--primary">
                                <div class="accordion__title-block">
                                    <h5 class="accordion__title">How SmartChain Services and Solutions can help</h5>
                                    <span class="accordion__close"></span>
                                </div>
                                <div class="accordion__text-block" style="display: none;">
                                    <p>
                                        SmartChain services and solutions can help the 3PL industry overcome these
                                        challenges by providing real-time visibility and control over the entire supply
                                        chain. This includes real-time monitoring of inventory levels, order status,
                                        delivery performance, and supplier performance. Additionally, SmartChain
                                        solutions can help optimize the supply chain by identifying inefficiencies and
                                        bottlenecks, and by implementing best practices and advanced technologies. By
                                        leveraging the power of SmartChain solutions, the 3PL industry can ensure
                                        reliable and efficient delivery of products and services, and ultimately improve
                                        customer satisfaction and profitability.
                                    </p>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-4 text-center text-xl-left">
                        <img class="truck-img"
                             src="{{asset('img/icons/Asset 14.svg')}}"
                             alt="img">
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection
