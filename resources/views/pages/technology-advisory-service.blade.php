@extends('layouts.app')

@section('content')
    <main class="main">

        <!-- section start-->
        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">Technology Advisory Services</h1>
                                <h5 class="text-white mt-3">
                                    Designed to provide expert guidance and support for technology-related initiatives
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end-->
        <section class="section service-details">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-xl-9">
                        <h5 class="service-details__title mb-4">Technology Advisory Services</h5>
                        <img class="service-details__img" src="{{asset('img/site/tas.webp')}}" alt="img">

                        <p>
                            SmartChain&#39;s Technology Advisory Services are designed to provide expert guidance and
                            support for
                            technology-related initiatives, such as support, enhancements, upgrades, and migrations. Our
                            team
                            of experts works with customers to develop a customized plan that meets their specific
                            technology
                            needs.
                        </p>
                        <div class="row top-20 offset-30" id="services-cards">
                            <div class="col-sm-6 col-xl-4">
                                <a href="#cloud-adoption" class="service-link">
                                    <div class="service-benefits">
                                        <ion-icon name="star"></ion-icon>
                                        <h6 class="service-benefits__title">Cloud Adoption</h6>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-xl-4">
                                <a href="#version-upgrade" class="service-link">
                                    <div class="service-benefits">
                                        <ion-icon name="star"></ion-icon>
                                        <h6 class="service-benefits__title">Version Upgrade</h6>
                                    </div>
                                </a>
                            </div>
                            <div class="col-sm-6 col-xl-4">
                                <a href="#automate" class="service-link">
                                    <div class="service-benefits">
                                        <ion-icon name="star"></ion-icon>
                                        <h6 class="service-benefits__title">Automate</h6>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xl-3 top-50 top-lg-0">
                        <div class="row">
                            <div class="col-md-6 col-lg-12 bottom-50">
                                <h5 class="blog__title">Our Services</h5>
                                <ul class="category-list list--reset">
                                    <li class="category-list__item"><a class="category-list__link"
                                                                       href="{{route('services.business-advisory-service')}}"><span>Business Consulting Service</span></a>
                                    </li>
                                    <li class="category-list__item"><a class="category-list__link" href="{{route('services.implementation-service')}}"><span>Implementation Service</span></a>
                                    </li>
                                    <li class="category-list__item item--active"><a class="category-list__link"
                                                                                    href="javascript:"><span>Technology Consulting Service</span></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-lg-12">
                                <div class="contact-trigger contact-trigger--style-2">
                                    <img class="contact-trigger__img" src="{{asset('img/contact_background.png')}}" alt="img">
                                    <h4 class="contact-trigger__title">Know more about SmartChain</h4>
                                    <p class="contact-trigger__text">
                                        Get in touch with us to know more about or company and how we operate...
                                    </p><a class="button button--white" href="{{route('contact-us')}}"><span>Know more about Us</span>
                                        <ion-icon name="arrow-forward"></ion-icon>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section bg--lgray" id="cloud-adoption">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-4">
                        <h4 class="bottom-20">Cloud Adoption
                        </h4>
                        <p>
                            SmartChain&#39;s Cloud Adoption Service is designed to
                            provide
                            expert guidance and support for logistics companies looking to adopt cloud technology. Our
                            team of
                            experts works with customers to develop a customized plan for seamless cloud adoption,
                            ensuring a
                            smooth transition to cloud computing and maximizing the benefits of cloud technology.
                        </p>

                    </div>
                    <div class="col-lg-7 col-xl-7 offset-xl-1 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits:</h6>
                        <ul class="list list--check list--reset">
                            <li class="list__item">Foundation Service: SmartChain&#39;s Cloud Adoption Service is a
                                foundation service that
                                provides the necessary support and guidance to ensure a seamless transition to cloud
                                computing.
                            </li>
                            <li class="list__item">Seamless Adoption: Our team of experts works with customers to
                                develop a customized plan
                                for cloud adoption, ensuring a smooth transition to the cloud and minimizing downtime
                                and
                                disruption to critical business processes.
                            </li>
                            <li class="list__item">On-Premise Implementation: In addition to cloud adoption, SmartChain
                                also provides on-
                                premise implementation services, including adoption and migration, to help customers
                                take
                                advantage of the latest technology and improve their operations.
                            </li>

                        </ul>
                    </div>
                    <a class="button--filled mt-3 ml-3 p-2 px-3 text-white" href="#services-cards">
                        <ion-icon size="large" name="arrow-up"></ion-icon>
                    </a>
                </div>
            </div>
        </section>
        <section class="section" id="version-upgrade">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-4">
                        <h4 class="bottom-20">Version Upgrade
                        </h4>
                        <p>
                            SmartChain&#39;s Version Upgrade for Blue Yonder service is designed to help customers
                            quickly and
                            efficiently upgrade their Blue Yonder system. Our approach uses a disciplined, agile
                            methodology to
                            ensure a smooth and low-cost upgrade process.
                        </p>

                    </div>
                    <div class="col-lg-7 col-xl-7 offset-xl-1 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits:</h6>
                        <ul class="list list--check list--reset">
                            <li class="list__item">
                                Disciplined, Agile Methodology: Our approach uses a disciplined, agile methodology that
                                takes customers through the upgrade process quickly and at a lower cost than their
                                original
                                implementation of Blue Yonder.
                            </li>
                            <li class="list__item">
                                Minimal Modification: Our approach is designed to keep modifications to a minimum,
                                ensuring that customers get the most out of their Blue Yonder system without extensive
                                modification.
                            </li>
                            <li class="list__item">
                                Thorough Understanding of Software: Our team of experts has a thorough understanding of
                                Blue Yonder software and knows how to help customers get the most from it without
                                modification.
                            </li>
                            <li class="list__item">
                                Streamlined Upgrade Path: By keeping modifications to a minimum, our approach makes the
                                future upgrade path even smoother and less expensive for customers.
                            </li>
                            <li class="list__item">
                                Rapid Upgrade: Our methodology and approach are designed to make the upgrade process
                                rapid, allowing customers to start seeing the benefits of their new system faster.
                            </li>

                        </ul>
                    </div>
                    <a class="button--filled mt-3 ml-3 p-2 px-3 text-white" href="#services-cards">
                        <ion-icon size="large" name="arrow-up"></ion-icon>
                    </a>
                </div>
            </div>
        </section>
        <section class="section bg--lgray" id="automate">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-4">
                        <h4 class="bottom-20">Automate
                        </h4>
                        <p>
                            Automation is a key aspect of our Technical Advisory services, which is designed to help
                            organizations leverage advanced technologies to sustain value and improve competitiveness.
                            Our
                            experts in software, strategy, and process engineering bring hundreds of complex customer
                            rollouts
                            and a deep understanding of the latest automation tools and technologies to help you achieve
                            your
                            goals.
                        </p>

                    </div>
                    <div class="col-lg-7 col-xl-7 offset-xl-1 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits:</h6>
                        <p>
                            Offers cost-effective, flexible and people-inclusive automation solutions
                            Utilizes robotics tools and technology for improved efficiency and competitiveness
                            Provides advisory for automation technology selection and implementation
                            Includes test automation for every process to ensure quality and accuracy
                            Navigates the hype to identify the right automation technologies for sustainable growth and
                            competitiveness.
                        </p>
                    </div>
                    <a class="button--filled mt-3 ml-3 p-2 px-3 text-white" href="#services-cards">
                        <ion-icon size="large" name="arrow-up"></ion-icon>
                    </a>
                </div>
            </div>
        </section>

        <div class="cta-block cta-block--style-2"><img class="img--bg" src="{{asset('img/site/cta.webp')}}" alt="bg"/>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8">
                        <div class="heading heading--white">
                            <h5 class="heading__title title-small">
                                Contact us today to learn more about how the SmartChain team can help your business
                                optimize
                                its supply chain operations.
                            </h5>
                        </div>
                    </div>
                    <div class="col-lg-4 text-lg-right"><a class="button button--white"
                                                           href="{{route('contact-us')}}"><span>Get in touch</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @include('includes.insights')


    </main>
@endsection
