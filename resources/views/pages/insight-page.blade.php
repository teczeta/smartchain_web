@extends('layouts.app')

@section('content')
    <main class="main">

        <!-- section start-->
        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">Insight</h1>
                                <h5 class="text-white mt-3">
                                    7 easy steps to successfully implement forecast
                                    Collaboration Automation
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end-->


        <section class="section blog">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-xl-9">
                        <div class="row">
                            <div class="col-12"><img class="blog-post__img" src="{{asset('img/i1.jpg')}}"
                                                     alt="img"/>
                                <h4 class="blog-post__title">7 easy steps to successfully implement forecast
                                    Collaboration Automation</h4>
                                <p>
                                    In today’s world of business, it is impossible to overlook the importance of the
                                    supply chain. It has
                                    clearly transformed from a supportive role into one of the most important business
                                    functions as well
                                    as the backbone of the current economy. Supply chain disruptions have in the recent
                                    past led to
                                    disproportionate price increases and supply shortages, creating a ripple effect with
                                    far-reaching
                                    consequences in the economy. So the importance of a robust supply chain cannot be
                                    understated.
                                </p>
                                <p>With the recent pandemic and the fluctuation and demand and supply, accurate
                                    forecasting has
                                    become more important than ever. Supply chain managers have started to leverage
                                    technological
                                    innovations and cutting-edge automation to better plan the material demand based
                                    on the needs of
                                    their individual systems. However, it is only the beginning. Like every innovation
                                    in the industry,
                                    forecast planning and collaboration in the supply chain require widespread
                                    acceptance and adoption
                                    across the industry. There is a sizeable section of the industry that needs to move
                                    away from manual
                                    processes, making it hard to forecast and plan accurately with the help of as many
                                    critical data
                                    points as it takes.</p>
                                <p class="text-filled mb-4">
                                    Collaboration requires participation from various teams, departments, and
                                    stakeholders such as the
                                    members of procurement, inventory management, planning, IT, and other departments.
                                </p>
                                <p>
                                    Only when the majority of these companies embrace the intelligent ways to forecast
                                    planning and
                                    collaboration, the supply chain can have efficiency and redundancies in work
                                    processes. They have a
                                    variety of tools at their disposal. However, companies need to customise their
                                    solution according to
                                    their needs. Successful transformation requires careful planning, research and
                                    implementation.
                                    Following are the steps involved in achieving this transformation
                                    Only when a majority of these companies embrace intelligent ways to forecast
                                    planning and
                                    collaboration, the supply chain can have efficiency and redundancies in work
                                    processes. They have a
                                    variety of tools at their disposal. However, companies need to customize their
                                    solution according to

                                    their needs. Successful transformation requires careful planning, research, and
                                    implementation.
                                    Following are the steps involved in achieving this transformation
                                </p>
                            </div>
                        </div>
                        <div class="row top-20">
                            <div class="col-6">
                                <div class="blog-post__date">2 December 2019</div>
                            </div>
                            <div class="col-6 text-right blog-post__date">Author Name
                            </div>
                        </div>


                    </div>
                    <div class="col-lg-4 col-xl-3 top-70 top-lg-0">
                        <div class="row">
                            <div class="col-md-6 col-lg-12 bottom-50">
                                <h5 class="blog__title">Categories</h5>
                                <ul class="category-list list--reset">
                                    <li class="category-list__item item--active"><a class="category-list__link"
                                                                                    href="#"><span>Insights</span></a>
                                    </li>
                                    <li class="category-list__item"><a class="category-list__link" href="#"><span>WhitePapers</span></a>
                                    </li>
                                </ul>
                            </div>


                            <div class="col-md-6 col-lg-12">
                                <div class="contact-trigger contact-trigger--style-2"><img class="contact-trigger__img"
                                                                                           src="img/contact_background.png"
                                                                                           alt="img"/>
                                    <h4 class="contact-trigger__title">Download the WhitePaper</h4>
                                    <p class="contact-trigger__text">
                                        Click here to download the white paper in PDF format
                                    </p><a class="button button--white"
                                           href="#"><span>Download</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section p-5 bg--lgray">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-lg-7">
                        <div class="heading">
                            <h3 class="heading__title">Get in <span
                                    class="color--green">touch</span></h3>
                        </div>

                        <p class="mt-4">
                            Contact us today to learn more about how the SmartChain team can help your business optimize
                            its supply chain operations.
                        </p>

                    </div>
                    <div class="col-lg-5 text-right">
                        <a class="button button--filled" href="#"><span>Contact us</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>


    </main>
@endsection
