@extends('layouts.app')

@section('content')
    <main class="main">

        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">Terms of Use</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row bottom-30">
                    <div class="col-xl-10 offset-xl-1">
                        <h3 class="bottom-0">Terms of Use</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-10 offset-xl-1">
                        <div class="container">
                            <p>The following terms and conditions govern the use of the SmartChain website. By accessing
                                and
                                using our website, you agree to be bound by these terms and conditions.</p>
                            <p>SmartChain reserves the right to modify these terms at any time, and your continued use
                                of the
                                website constitutes acceptance of the revised terms.</p>
                            <p>The content on the SmartChain website, including text, images, and other materials, is
                                the property
                                of SmartChain and is protected by copyright and trademark laws. You may not copy,
                                distribute, or
                                use this content without the express written consent of SmartChain.</p>
                            <p>SmartChain makes no representations or warranties of any kind, express or implied, about
                                the
                                completeness, accuracy, reliability, suitability or availability with respect to the
                                website or the
                                information, products, services, or related graphics contained on the website for any
                                purpose. Any
                                reliance you place on such information is therefore strictly at your own risk.</p>
                            <p>SmartChain is not responsible for any damages, including without limitation, indirect or
                                consequential damages, arising from the use of the website or the information contained
                                on it.</p>
                            <p>By accessing and using the SmartChain website, you agree to indemnify and hold harmless
                                SmartChain and its affiliates from any claims, damages, or expenses arising from your
                                use of the
                                website.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection
