@extends('layouts.app')

@section('content')
    <main class="main">

        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">Page Title</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row bottom-30">
                    <div class="col-xl-10 offset-xl-1">
                        <h3 class="bottom-0">Page Under Development</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-10 offset-xl-1">
                        <div class="container">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. A ab asperiores assumenda cum,
                                cupiditate dicta dolor dolorem error, excepturi expedita explicabo fuga hic id illo in
                                incidunt natus nostrum officia omnis porro possimus provident quaerat, repudiandae sed
                                sequi sunt temporibus ullam unde ut voluptatum. Amet architecto consequatur excepturi
                                facere harum itaque iusto, labore modi molestias perspiciatis placeat quaerat qui quis
                                repudiandae sed soluta ut, veritatis. Assumenda autem dicta ea laboriosam non, odit
                                provident quasi, sed, sequi tempora temporibus ut vitae? Beatae debitis dicta distinctio
                                et expedita explicabo, facere hic illo incidunt laborum molestias nulla odio perferendis
                                quam, quo rerum totam. Accusamus accusantium cum distinctio nam neque odit perspiciatis
                                quae repudiandae rerum? Atque commodi cupiditate enim illum incidunt quibusdam sunt
                                suscipit? Accusantium consequatur corporis, cumque cupiditate eligendi eos est labore
                                laboriosam libero provident, reiciendis repudiandae sunt tenetur. Adipisci commodi
                                corporis distinctio enim eos explicabo hic illum inventore, iusto labore natus provident
                                quas quos, repellat sed soluta, sunt temporibus vel. Amet delectus dolor et excepturi
                                facilis molestiae, non porro ratione sint tenetur vel vitae. Alias asperiores delectus
                                dicta, dolores eaque earum et eveniet excepturi expedita fugit id magni minima mollitia
                                natus, necessitatibus nemo nihil nostrum reiciendis sapiente, tempora tenetur unde vel
                                velit veniam voluptate voluptatem voluptates? Ad asperiores atque cumque delectus
                                distinctio dolor doloremque ducimus earum error eum explicabo facilis iure laborum
                                maiores optio porro, ratione similique sit sunt suscipit temporibus totam ut? Animi
                                atque blanditiis, doloremque est ex explicabo facere fuga fugit, illum itaque magni modi
                                nam nobis obcaecati officia perferendis possimus quia quidem quisquam ratione repellat
                                similique sint, sunt ullam velit vitae voluptatem. Alias aliquam aperiam consequuntur
                                corporis cum delectus distinctio dolore dolores ea excepturi explicabo fugit illum
                                impedit incidunt ipsa iste libero magni minima natus neque nesciunt nisi non nulla
                                officia placeat quas quibusdam quod quos recusandae reiciendis reprehenderit suscipit,
                                ullam veniam voluptas voluptates voluptatibus voluptatum! Accusamus dolorem, eos esse,
                                exercitationem facilis impedit in iusto labore magnam maiores obcaecati officiis quaerat
                                quas quibusdam quo ratione voluptatibus! Amet commodi, eos esse explicabo magnam sint
                                totam? Architecto asperiores eaque eum, explicabo facilis laudantium magnam nemo odio
                                officiis pariatur ratione, ullam voluptates. At dignissimos dolore dolores explicabo
                                illum laboriosam molestias pariatur quis sapiente voluptatibus? Animi atque eum harum
                                modi nihil non nulla, officiis provident quam quisquam? Animi architecto eos, eum hic
                                officia sed similique sint. Culpa doloremque eius eos est iste mollitia officiis?
                                Accusantium alias animi at consequuntur dolorum ducimus ea error eum excepturi
                                exercitationem facilis ipsa itaque laboriosam nostrum nulla numquam possimus qui quo
                                quod recusandae repellat, repudiandae veniam! Atque culpa debitis dicta explicabo
                                facilis iste laborum magnam nisi perspiciatis quidem quos, suscipit vitae? A ab adipisci
                                amet aspernatur assumenda consequuntur cumque deserunt ducimus earum facilis in magnam
                                nam nisi nulla odio officiis, omnis provident quae quaerat, sint totam voluptate
                                voluptatum. Alias cum inventore nihil non. Adipisci alias aliquid aut delectus deleniti
                                dignissimos earum incidunt inventore, itaque magnam magni, natus nulla perspiciatis
                                sapiente sed suscipit tempore unde velit veritatis voluptatibus! Amet consequatur
                                deserunt dolorum eius et laborum maxime minima veniam vitae.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection
