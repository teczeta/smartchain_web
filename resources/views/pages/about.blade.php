@extends('layouts.app')

@section('content')
    <main class="main">

        <!-- section start-->
        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">About Us</h1>
                                <h5 class="text-white mt-3">
                                    Our team is our greatest strength
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end-->

        <section class="section  about-section bg--lgray pb-0">
            <div class="container">
                <div class="row bottom-70">
                    <div class="col-12">
                        <div class="heading heading--center"><span class="heading__pre-title">SmartChain</span>
                            <h3 class="heading__title">Who we are?</h3><span class="heading__layout">Vision</span>
                        </div>
                    </div>
                </div>
                <div class="row align-items-stretch justify-content-center">
                    <div class="col-md-4 col-sm-12 my-2">
                        <div class="icon-item shadow-sm">
                            <h4 class="icon-item__title">Our Vision</h4>
                            <div class="icon-item__img"><img src="{{asset('img/icons/Asset 10.svg')}}"></div>

                            <p class="icon-item__text">
                                &quot;Our vision at SmartChain is to revolutionize the way businesses approach supply
                                chain management
                                by providing expert consulting and technology solutions that drive efficiency, reduce
                                costs, and
                                improve overall performance. We are committed to fostering a diverse and inclusive work
                                environment, and actively promoting gender sensitivity within our company and in our
                                interactions
                                with clients.&quot;
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 my-2">
                        <div class="icon-item shadow-sm">
                            <h4 class="icon-item__title">Our Mission</h4>
                            <div class="icon-item__img"><img src="{{asset('img/icons/Asset 9.svg')}}"></div>

                            <p class="icon-item__text">
                                &quot;Our Mission at SmartChain is to establish ourselves as a trusted partner for
                                businesses in their supply
                                chain journey. We aim to achieve this by providing a holistic approach to supply chain
                                management
                                through our extensive portfolio of services and solutions, and by delivering measurable
                                results for
                                our clients.”
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 my-2">
                        <div class="icon-item shadow-sm">
                            <h4 class="icon-item__title">Our Values</h4>
                            <div class="icon-item__img"><img src="{{asset('img/icons/Asset 11.svg')}}"></div>

                            <p class="icon-item__text">
                                Our value system at SmartChain reflects our commitment to delivering exceptional
                                services and
                                solutions to our clients. These values are at the heart of everything we do at
                                SmartChain, and they
                                guide us in our mission to deliver the best possible solutions to our clients.
                            </p>
                            <div class="text-left mt-2">
                                <ul class="list list--check list--reset">
                                    <li class="list__item">Intelligence</li>
                                    <li class="list__item">Transparency</li>
                                    <li class="list__item">Teamwork</li>
                                    <li class="list__item">Sustainability</li>
                                    <li class="list__item">Gender Empowerment</li>
                                </ul>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="section testimonials bg--lgray">
            <div class="testimonials__bg"><img class="section--bg t50 r0" src="{{asset('img/testimonials-bg.png')}}"
                                               alt="img"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="heading heading--white">
                            <h3 class="heading__title">
                                About <span class="color--green">SmartChain</span>
                            </h3>
                            <em class="pt-3">
                                We believe that a diverse team brings unique perspectives and is a key driver of
                                creativity and innovation.
                            </em>
                        </div>
                        <img class="testimonials__img" src="{{asset('img/site/about.webp')}}" alt="img">
                    </div>
                    <div class="col-lg-7 offset-lg-1">
                        <p class="mt-5">
                            At SmartChain, we are proud to have assembled a team of highly experienced and knowledgeable
                            logistics experts. Our team is composed of seasoned professionals with over 200+ years of
                            combined
                            experience in the supply chain industry. Our team members have held key roles in leading
                            supply
                            chain solutions and have successfully implemented solutions for clients across a wide range
                            of
                            industries and segments, from small businesses to Fortune 500 companies.
                        </p>
                        <p class="">
                            Our team members are domain experts, solution architects, technical architects, and
                            consultants
                            who bring a combination of deep technical expertise, operational experience, and broad
                            business
                            knowledge to deliver lasting results for our clients. All our team members come from
                            operational
                            backgrounds and have been through rigorous training, ensuring they have the knowledge and
                            skills
                            needed to provide expert guidance and support to our clients.
                        </p>
                        <p class="mb-5">
                            We are committed to fostering a culture of inclusivity and diversity within our team, and
                            actively
                            promote gender sensitivity in our interactions with clients and within our company. We
                            believe that
                            a diverse team brings unique perspectives and is a key driver of creativity and innovation.
                        </p>
                    </div>
                </div>
            </div>
        </section>

        <section class="section p-5">
            <div class="container">
                <div class="row justify-content-center align-items-center">
                    <div class="col-lg-7">
                        <div class="heading">
                            <h3 class="heading__title">Get in <span
                                    class="color--green">touch</span></h3>
                        </div>

                        <p class="mt-4">
                            Contact us today to learn more about how the SmartChain team can help your business optimize
                            its supply chain operations.
                        </p>

                    </div>
                    <div class="col-lg-5 text-right">
                        <a class="button button--filled" href="{{route('contact-us')}}"><span>Contact us</span>
                        </a>
                    </div>
                </div>
            </div>
        </section>


    </main>
@endsection
