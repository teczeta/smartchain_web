@extends('layouts.app')

@section('content')
    <main class="main">

        <!-- section start-->
        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">Business Consulting Services</h1>
                                <h5 class="text-white mt-3">
                                    Transform Your Business with SmartChain's Business Consulting Services
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end-->
        <section class="section service-details">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-xl-9">
                        <h5 class="service-details__title mb-4">Business Consulting Service</h5>
                        <img class="service-details__img" src="{{asset('img/site/ba.webp')}}" alt="img">

                        <h6>
                            &quot;Transform Your Business with SmartChain&#39;s Business Consulting Services&quot;
                        </h6>
                        <p>
                            Our Business Advisory Services provide a comprehensive approach to transform your business
                            operations. Whether you are looking to improve your business performance, increase
                            efficiency,
                            reduce costs, enhance customer satisfaction, or increase revenue, our team of experts can
                            help. Our
                            services include product evaluation, where we assess your current systems and processes to
                            identify
                            areas for improvement, as well as develop a roadmap for improvement, which lays out a clear
                            plan
                            of action for achieving your business goals.
                        </p>
                        <p>
                            Additionally, we offer transformation services, which include the implementation of the
                            necessary
                            changes to achieve those goals. We specialize in assessment, strategy, and transformation of
                            your
                            logistics operations to help you achieve your business goals.
                        </p>
                        <em>With our team of experts, we can help you to take your business to the next level.</em>
                    </div>
                    <div class="col-lg-4 col-xl-3 top-50 top-lg-0">
                        <div class="row">
                            <div class="col-md-6 col-lg-12 bottom-50">
                                <h5 class="blog__title">Our Services</h5>
                                <ul class="category-list list--reset">
                                    <li class="category-list__item item--active"><a class="category-list__link"
                                                                                    href="javascript:"><span>Business Consulting Service</span></a>
                                    </li>
                                    <li class="category-list__item"><a class="category-list__link" href="{{route('services.implementation-service')}}"><span>Implementation Service</span></a>
                                    </li>
                                    <li class="category-list__item"><a class="category-list__link" href="{{route('services.technology-advisory-service')}}"><span>Technology Consulting Service</span></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-lg-12">
                                <div class="contact-trigger contact-trigger--style-2">
                                    <img class="contact-trigger__img" src="{{asset('img/contact_background.png')}}" alt="img">
                                    <h4 class="contact-trigger__title">Know more about SmartChain</h4>
                                    <p class="contact-trigger__text">
                                        Get in touch with us to know more about or company and how we operate...
                                    </p><a class="button button--white" href="{{route('contact-us')}}"><span>Know more about Us</span>
                                        <ion-icon name="arrow-forward"></ion-icon>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section bg--dgray"><img class="section--bg t50 r0" src="{{asset('img/testimonials-bg.png')}}" alt="img">
            <div class="container">
                <div class="row bottom-70">
                    <div class="col-12">
                        <div class="heading heading--white"><span class="heading__pre-title">Business Consulting Services</span>
                            <h3 class="heading__title">How our Business Consulting Solutions can impact your
                                Business</h3>
                        </div>
                    </div>
                </div>
                <div class="row offset-50">
                    <div class="col-md-6 col-xl-4">
                        <div class="icon-item icon-item--white">
                            <div class="icon-item__count"><span>01</span></div>
                            <h3 class="icon-item__title">Product evaluation</h3>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-4">
                        <div class="icon-item icon-item--white">
                            <div class="icon-item__count"><span>02</span></div>
                            <h3 class="icon-item__title">Develop Roadmaps</h3>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-4">
                        <div class="icon-item icon-item--white">
                            <div class="icon-item__count"><span>03</span></div>
                            <h3 class="icon-item__title"> Business Transformation</h3>
                        </div>
                    </div>

                </div>
            </div>
        </section>

        <section class="section bg--lgray features-front_6">
            <div class="container">
                <div class="row bottom-50">
                    <div class="col-12">
                        <div class="heading"><span class="heading__pre-title">Features</span>
                            <h3 class="heading__title">How our <span class="color--green">Advisory services</span> <br>
                                can impact your business</h3>
                            <span class="heading__layout layout--lgray">Features</span>
                        </div>
                    </div>
                </div>
                <div class="row offset-50">
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="info-item">
                            <div class="info-item__img">
                                <ion-icon name="bar-chart"></ion-icon>
                            </div>
                            <div class="info-item__details">
                                <strong>Improve business performance</strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="info-item">
                            <div class="info-item__img">
                                <ion-icon name="checkmark-circle-outline"></ion-icon>
                            </div>
                            <div class="info-item__details">
                                <strong>Increase efficiency</strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="info-item">
                            <div class="info-item__img">
                                <ion-icon name="cash-outline"></ion-icon>
                            </div>
                            <div class="info-item__details">
                                <strong>Reduce costs</strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="info-item">
                            <div class="info-item__img">
                                <ion-icon name="checkmark-circle-outline"></ion-icon>
                            </div>
                            <div class="info-item__details">
                                <strong>Enhance customer satisfaction</strong>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="info-item">
                            <div class="info-item__img">
                                <ion-icon name="caret-up-circle-outline"></ion-icon>
                            </div>
                            <div class="info-item__details">
                                <strong>Increase revenue</strong>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <div class="cta-block cta-block--style-2"><img class="img--bg" src="{{asset('img/site/cta.webp')}}" alt="bg"/>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8">
                        <div class="heading heading--white">
                            <h5 class="heading__title title-small">
                                Contact us today to learn more about how the SmartChain team can help your business optimize
                                its supply chain operations.
                            </h5>
                        </div>
                    </div>
                    <div class="col-lg-4 text-lg-right"><a class="button button--white"
                                                           href="{{route('contact-us')}}"><span>Get in touch</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @include('includes.insights')


    </main>
@endsection
