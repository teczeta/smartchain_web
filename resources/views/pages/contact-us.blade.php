@extends('layouts.app')

@section('content')
    <main class="main">
        <!-- section start-->
        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">Contact Us</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end-->
        <!-- section start-->
        <section class="section contacts py-5 my-5">
            <img class="contacts__bg" src="{{asset('img/contacts-map.png')}}" alt="map"/>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h5 class="contacts__title">Office Address</h5>
                        <div class="contacts-item">
                            <div class="contacts-item__details"><span>12, Scott Court, <br>
                            Chester Brook, PA 19087</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <form class="form contact-form" action="javascript:void(0);" method="post"
                              autocomplete="off">
                            <div class="row">
                                <div class="col-12">
                                    <h5 class="contact-form__subtitle">Get in Touch</h5>
                                </div>
                                <div class="col-md-6">
                                    <input class="form__field" type="text" name="name" placeholder="Your Name"/>
                                </div>
                                <div class="col-md-6">
                                    <input class="form__field" type="email" name="email" placeholder="Your Email"/>
                                </div>
                                <div class="col-md-6">
                                    <input class="form__field" type="tel" name="phone" placeholder="Your Phone"/>
                                </div>
                                <div class="col-md-6">
                                    <input class="form__field" type="text" name="subject" placeholder="Subject"/>
                                </div>
                                <div class="col-12">
                                    <textarea class="form__field form__message message--large" name="message"
                                              placeholder="Text"></textarea>
                                </div>
                                <div class="col-12">
                                    <button class="button button--green" type="submit"><span>Send message</span>

                                    </button>
                                </div>
                                <div class="col-12">
                                    <div class="alert alert--success alert--filled">
                                        <div class="alert__icon">

                                        </div>
                                        <p class="alert__text"><strong>Well done!</strong> Your form has been sent</p>
                                        <span class="alert__close">
													</span>
                                    </div>
                                    <div class="alert alert--error alert--filled">
                                        <div class="alert__icon">

                                        </div>
                                        <p class="alert__text"><strong>Oh snap!</strong> Your form has not been sent</p>
                                        <span class="alert__close">
													</span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end-->


    </main>
@endsection
