@extends('layouts.app')

@section('content')
    <main class="main">
        <!-- promo start-->
        <div class="front-promo">
            <div class="front-promo__layout"></div>
            <picture>
                <source srcset="{{asset('img/site/hero-home.webp?v=1.1')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero-home.webp?v=1.1')}}" alt="img"/>
            </picture>
            <div class="align-container">
                <div class="align-container__item">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-lg-9">
                                <img class="hero-logo" src="{{asset('img/logo/logo-hero.svg')}}" alt="">
                                <h2 class="front-promo__title">
                                    Expert Consulting and Technology Solutions for Your Supply Chain Needs
                                </h2>
                                <p class="front-promo__subtitle">
                                    "Unlock the full potential of your supply chain with expert consulting and
                                    technology
                                    solutions from Smartchain. Contact us today to learn more about how Smartchain
                                    can
                                    take your supply chain to the next level."
                                </p>
                                <a class="button button--outline" href="{{route('contact-us')}}">
                                    <span>Contact us</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- promo end-->
        <section class="section section-1 bg--lgray">
            <div class="container">
                <div class="row">
                    <h3 class="heading__title pb-5">Unleashing the Power of <br class="d-none d-lg-block"/>
                        <span class="color--green">Next-Generation Supply Chain</span> Technologies</h3>
                    <p>
                        SmartChain is a supply chain consulting and technology company that helps businesses optimize
                        their operations through expert consulting and cutting-edge technology solutions. Our team of
                        highly skilled professionals has decades of experience in implementing leading supply chain
                        solutions for customers across a wide range of industries.
                    </p>
                </div>
            </div>
            <div class="container">
                <div class="row align-items-center justify-contents-center">
                    <div class="col-12">
                        <div class="row interact">
                            <div class="col-lg-4 col-md-12">
                                <a href="{{route('services.business-advisory-service')}}">
                                    <div class="items blue">
                                        <div class="circle-container">
                                            <img class="mini-icon" src="{{asset('img/icon-3.svg')}}" alt="">
                                            <h5> Business Consulting Services</h5>

                                            <p class="mb-0">
                                                Product Evaluation,
                                                Develop Roadmap,
                                                Templates.
                                            </p>
                                            <p class="text-cta mt-2">Click to know more...</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <a href="{{route('services.implementation-service')}}">
                                    <div class="items red">
                                        <div class="circle-container">
                                            <img class="mini-icon" src="{{asset('img/icon-2.svg')}}" alt="">
                                            <h5>Implementation Services</h5>

                                            <p class="mb-0">
                                                POC/Rollouts/Upgrades,
                                                Development,
                                                Integration,
                                                Testing,
                                                Training,
                                                Support.
                                            </p>
                                            <p class="text-cta mt-2">Click to know more...</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="col-lg-4 col-md-12">
                                <a href="{{route('services.technology-advisory-service')}}">
                                    <div class="items orange">
                                        <div class="circle-container">
                                            <img class="mini-icon" src="{{asset('img/icon-1.svg')}}" alt="">
                                            <h5> Technology Consulting Services</h5>
                                            <p class="mb-0">
                                                Cloud Adoption,
                                                Upgrade,
                                                Migration,
                                                Automate.
                                            </p>
                                            <p class="text-cta mt-2">Click to know more...</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section start-->
        <section class="section pb-0 pt-5 section-2">
            <div class="container bottom-70">
                <h3 class="heading__title pb-5">Transforming your Business with <br>
                    <span class="color--green">Cutting-Edge Supply Chain</span> Solutions</h3>
                <p class="bottom-0"><strong>SmartChain focuses on providing solutions within the planning and execution
                        space of the supply chain. </strong> We work closely with customers to transform their
                    businesses and
                    generate real change.</p>

                <div class="tab-wrapper my-5 mx-3">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="planing-tab" data-toggle="tab" data-target="#planing"
                                    type="button" role="tab" aria-controls="planing" aria-selected="true">Supply Chain
                                <br class="d-lg-none"/>
                                Planning
                            </button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="execution-tab" data-toggle="tab" data-target="#execution"
                                    type="button" role="tab" aria-controls="execution" aria-selected="false">Supply
                                Chain <br class="d-lg-none"/> Execution
                            </button>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="planing" role="tabpanel"
                             aria-labelledby="planing-tab">
                            <div class="row no-gutters">
                                <div class="col">
                                    <div class="card bg--lgray shadow border-0">
                                        <p class="px-4 pt-4">
                                            Our supply chain planning solutions help customers optimize their operations
                                            and drive efficiency. Our offerings include integrated business planning and
                                            sales and operations planning, as well as demand planning and forecasting,
                                            inventory planning optimization, supply network planning, production
                                            scheduling, and customer and supplier collaboration.
                                        </p>
                                        <div class="row align-items-center">
                                            <div class="col">
                                                <img src="{{asset('img/icons/kinaxis.svg')}}" alt="">
                                            </div>
                                            <div class="col text-right">
                                                <a class="px-4 pb-4"
                                                   href="{{route('solution.supply-chain-planning')}}">Know
                                                    more...</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col">
                                    <div class="card card-2 bg--blue border-0">
                                        <div class="p-4">
                                            <h4 class="bottom-20">Supply Chain Planning</h4>
                                            <ul class="list list--check list--reset">
                                                <li class="list__item">Integrated Business Planning / S&OP</li>
                                                <li class="list__item">Demand Planning & Forecasting</li>
                                                <li class="list__item">Inventory Planning Optimization</li>
                                                <li class="list__item">Supply Network Planning</li>
                                                <li class="list__item">Production Scheduling</li>
                                                <li class="list__item">Customer & Supplier Collaboration</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane fade" id="execution" role="tabpanel" aria-labelledby="execution-tab">
                            <div class="row no-gutters">
                                <div class="col">
                                    <div class="card bg--lgray shadow border-0">
                                        <p class="px-4 pt-4">
                                            Our supply chain execution solutions help customers optimize their logistics
                                            and supply chain processes. Our offerings include warehouse management
                                            system and warehouse logistics management solutions, transportation
                                            management system solutions, and warehouse automation solutions.
                                        </p>
                                        <div class="row align-items-center">
                                            <div class="col">
                                                <img src="{{asset('img/icons/blue_yonder.svg')}}" alt="">
                                            </div>
                                            <div class="col text-right">
                                                <a class="px-4 pb-4"
                                                   href="{{route('solution.supply-chain-execution')}}">Know
                                                    more...</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="card card-2 bg--blue border-0">
                                        <div class="p-4">
                                            <h4 class="bottom-20">Supply Chain Execution</h4>
                                            <ul class="list list--check list--reset">
                                                <li class="list__item">Transportation Planning and Allocation</li>
                                                <li class="list__item">Transportation Optimisation</li>
                                                <li class="list__item">Freight Audit &amp; Payment</li>
                                                <li class="list__item">Warehouse Automation</li>
                                                <li class="list__item">Labour Management</li>
                                                <li class="list__item">Transport Modelling</li>
                                                <li class="list__item">Fleet Management</li>
                                                <li class="list__item">Carrier Onboarding</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
        <!-- section end-->
        <section class="section testimonials">
            <div class="testimonials__bg"><img class="section--bg t50 r0" src="{{asset('img/testimonials-bg.png')}}"
                                               alt="img"/>
            </div>
            <div class="container">
                <div class="row mt-lg-5">
                    <div class="col-lg-5">
                        <div class="heading heading--white">
                            <h3 class="heading__title mb-4">How we do it?</h3>
                            <p>
                                We apply a combination of deep technical expertise, operational experience, and broad
                                business knowledge to deliver lasting results.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 offset-lg-1">
                        <div class="testimonials-slider">
                            <div class="testimonials-slider__item">
                                <h5>Consultation</h5>
                                <p>
                                    The first step in the process would be to conduct a consultation with the
                                    organization to understand its logistics needs and challenges. This could involve
                                    analyzing the organization's current logistics operations, identifying areas for
                                    improvement, and discussing potential solutions.
                                </p>

                            </div>
                            <div class="testimonials-slider__item">
                                <h5>Solution design</h5>
                                <p>
                                    Based on the consultation, SmartChain would work with the organization to design a
                                    customized solution that addresses its specific needs and goals. This could involve
                                    implementing new technologies, optimizing existing processes, or developing a new
                                    supply chain strategy.
                                </p>

                            </div>
                            <div class="testimonials-slider__item">
                                <h5>Implementation</h5>
                                <p>
                                    Once the solution has been designed, SmartChain would work with the organization to
                                    implement it. This could involve providing support with proof-of-concept testing,
                                    rollouts, and upgrades, as well as developing custom solutions, integrating systems,
                                    and conducting testing to ensure that everything is working smoothly.
                                </p>

                            </div>
                            <div class="testimonials-slider__item">
                                <h5>Maintenance and support</h5>
                                <p>
                                    After the solution has been implemented, SmartChain would provide ongoing
                                    maintenance and support to ensure that it is running smoothly and effectively. This
                                    could include providing training and support for end users, as well as offering
                                    technology advisory services to help the organization enhance, upgrade, or migrate
                                    its systems as needed.
                                </p>

                            </div>
                            <div class="testimonials-slider__item">
                                <h5>Sustainable Solutions</h5>
                                <p>
                                    SmartChain values sustainability and incorporates it into our operations and support
                                    of clients. We
                                    partner with PlanetWise to ensure compliance with sustainability regulations and
                                    guidelines. Our
                                    approach not only meets requirements, but is a part of our values. This results in
                                    reduced carbon
                                    footprint and promotes energy efficiency, providing our clients with a sustainable
                                    supply chain
                                    solution. By working with us, clients receive top-notch logistics technology
                                    services and contribute to
                                    a more sustainable future.
                                </p>

                            </div>
                        </div>
                        <div class="testimonials__nav"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end-->
        <section class="section section-1 section-3 bg--lgray">
            <img class="bg-icon" src="{{asset('img/logo/icon-color.svg')}}" alt="bg">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <h3 class="heading__title">
                            <span class="color--green">Industries</span> <br>
                            we Work</h3>
                    </div>
                    <div class="offset-md-1 col-md-8">
                        <p class="bottom-0">All businesses have unique supply chain needs and challenges. Our team
                            experience and expertise enable us to address these needs and help organizations in these
                            industries optimize their logistics operations.</p>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row align-items-stretch">
                    <div class="col-md-6 col-sm-12 my-3">
                        <div class="mx-3 shadow card-row">
                            <img class="industry-icon" src="{{asset('img/icons/Asset 3.svg')}}" alt="">
                            <h5 class="industry-title"> Automotive</h5>
                            <p class="industry-text">The automotive industry is facing challenges in keeping up with
                                customer expectations due to the rapid pace of innovation and changing supply
                                sources and tariffs.</p>
                            <a href="{{route('why.industries-we-work')}}">Read more...</a>

                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 my-3">
                        <div class="mx-3 shadow card-row">
                            <img class="industry-icon" src="{{asset('img/icons/Asset 6.svg')}}" alt="">
                            <h5 class="industry-title"> Consumer Packaged Goods</h5>
                            <p class="industry-text">
                                The CPG industry is facing increasing complexity and changing buyer
                                behavior, leading to rising supply chain costs and high customer
                                expectations for product sustainability and competitive prices.
                            </p>
                            <a href="{{route('why.industries-we-work')}}">Read more...</a>


                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 my-3">
                        <div class="mx-3 shadow card-row">
                            <img class="industry-icon" src="{{asset('img/icons/Asset 13.svg')}}" alt="">
                            <h5 class="industry-title"> High Tech</h5>
                            <p class="industry-text">
                                The High Tech industry faces complex, global supply chains and increased
                                competition, making it challenging for supply chain planning. Risks
                                including trade/tariff uncertainties and fast-paced innovation add to
                                these difficulties.
                            </p>
                            <a href="{{route('why.industries-we-work')}}">Read more...</a>


                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 my-3">
                        <div class="mx-3 shadow card-row">
                            <img class="industry-icon" src="{{asset('img/icons/Asset 2.svg')}}" alt="">


                            <h5 class="industry-title"> Life Sciences</h5>
                            <p class="industry-text">The life sciences supply chain is complex and extended, and
                                companies must be prepared for risks to critical medications and services. The COVID-19
                                pandemic has increased complexity and volatility in the industry.</p>
                            <a href="{{route('why.industries-we-work')}}">Read more...</a>

                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 my-3">
                        <div class="mx-3 shadow card-row">
                            <img class="industry-icon" src="{{asset('img/icons/Asset 1.svg')}}" alt="">


                            <h5 class="industry-title">Manufacturing</h5>
                            <p class="industry-text">
                                The Manufacturing industry faces unique challenges in the logistics function, such as
                                managing complex supply chains, ensuring product quality, and reducing costs.
                            </p>
                            <a href="{{route('why.industries-we-work')}}">Read more...</a>

                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 my-3">
                        <div class="mx-3 shadow card-row">
                            <img class="industry-icon" src="{{asset('img/icons/Asset 14.svg')}}" alt="">


                            <h5 class="industry-title">Third Party Logistics (3PL)</h5>
                            <p class="industry-text">
                                The 3PL industry faces several challenges in the logistics function, including increased
                                demand for cost-effective solutions, increased competition, and the need to work with
                                multiple business partners.
                            </p>
                            <a href="{{route('why.industries-we-work')}}">Read more...</a>

                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-5">
            <div class="container-fluid mx-md-5 px-md-5">
                <div class="row align-items-center justify-content-center">
                    <div class="col-lg-8 col-md-12">
                        <h3 class="heading__title pb-5 text-white">Why Choose Smartchain?</h3>
                        <div class="tab-wrapper">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link active" id="exp-tab" data-toggle="tab"
                                            data-target="#exp"
                                            type="button" role="tab" aria-controls="exp" aria-selected="true">
                                        Expertise
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="comp-tab" data-toggle="tab"
                                            data-target="#comp"
                                            type="button" role="tab" aria-controls="comp" aria-selected="false">
                                        Solutions and Integration
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="lasting-tab" data-toggle="tab"
                                            data-target="#lasting"
                                            type="button" role="tab" aria-controls="lasting" aria-selected="false">
                                        Customer Focus
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="benefits-tab" data-toggle="tab"
                                            data-target="#benefits"
                                            type="button" role="tab" aria-controls="benefits" aria-selected="false">
                                        Business Benefits
                                    </button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link" id="sustainability-tab" data-toggle="tab"
                                            data-target="#sustainability"
                                            type="button" role="tab" aria-controls="benefits" aria-selected="false">
                                        Sustainability
                                    </button>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="exp" role="tabpanel"
                                     aria-labelledby="exp-tab">
                                    <div class="row no-gutters">
                                        <div class="col">
                                            <div class="card bg--blue shadow border-0">
                                                <p class="p-4">
                                                    SmartChain has a team of highly skilled professionals with decades
                                                    of experience in implementing leading supply chain solutions for
                                                    customers across a wide range of industries. This expertise allows
                                                    SmartChain to provide expert consulting and guidance that can help
                                                    customers optimize their logistics operations and drive efficiency.
                                                </p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane fade" id="comp" role="tabpanel"
                                     aria-labelledby="comp-tab">
                                    <div class="row no-gutters">
                                        <div class="col">
                                            <div class="card bg--blue shadow border-0">
                                                <p class="p-4">
                                                    SmartChain offers a range of services and solutions that cover all
                                                    aspects of the supply chain, from planning to execution. This allows
                                                    SmartChain to provide a comprehensive approach to addressing
                                                    customers' logistics needs, ensuring that all areas of the supply
                                                    chain are optimized.
                                                </p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane fade" id="lasting" role="tabpanel"
                                     aria-labelledby="lasting-tab">
                                    <div class="row no-gutters">
                                        <div class="col">
                                            <div class="card bg--blue shadow border-0">
                                                <p class="p-4">
                                                    Maximizing Blue Yonder WMS/TMS investments and minimizing costs
                                                    while providing competitive offerings and attractive pricing models.
                                                    Offering the flexibility to adapt to customer experience processes
                                                    and tools and using Agile or Waterfall project management approaches
                                                    based on circumstances and customer preferences.
                                                </p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane fade" id="benefits" role="tabpanel"
                                     aria-labelledby="benefits-tab">
                                    <div class="row no-gutters">
                                        <div class="col">
                                            <div class="card bg--blue shadow border-0">
                                                <p class="p-4">
                                                    SmartChain's team applies a combination of deep technical expertise,
                                                    operational experience, and broad business knowledge to deliver
                                                    lasting results for its customers. This means that customers can
                                                    expect to see real, measurable improvements in their logistics
                                                    operations that will have a long-term impact on their business.
                                                </p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane fade" id="sustainability" role="tabpanel"
                                     aria-labelledby="sustainability-tab">
                                    <div class="row no-gutters">
                                        <div class="col">
                                            <div class="card bg--blue shadow border-0">
                                                <p class="p-4">
                                                    At SmartChain, we are committed to promoting sustainability in all
                                                    aspects of our operations and services. With the help of our
                                                    sustainability partner, PlanetWise, we ensure that we are fully
                                                    compliant with all sustainability regulations and guidelines.
                                                    Our approach to sustainability is not just about meeting regulatory
                                                    requirements, but about incorporating it into our value system.
                                                </p>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <ul class="list list--check list--reset text-white mt-5 ml-5">
                            <li class="list__item">Next gen technologies</li>
                            <li class="list__item">End to end solution provider</li>
                            <li class="list__item">Customized solutions</li>
                            <li class="list__item">Cost savings</li>
                            <li class="list__item">Improved efficiency</li>
                            <li class="list__item">Increased customer satisfaction</li>
                            <li class="list__item">Increased revenue</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="section section-red ">
            <div class="testimonials__bg">
            </div>
            <img class="d-none d-lg-block lady-img" src="{{asset('img/lady.png')}}" alt="">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-lg-2 d-none d-lg-flex">
                        <img class="" src="{{asset('img/quote-outline.svg')}}" alt="">
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="heading heading--white">
                            <h3 class="heading__title mb-5">We Believe</h3>
                            <p>
                                At SmartChain, we believe in building a diverse and inclusive team that reflects the
                                communities we serve. We are committed to creating an environment where all team members
                                can bring their unique backgrounds, experiences, and perspectives to the table. In
                                addition, we are committed to empowering women in the workplace and providing equal
                                opportunities for career advancement. We believe that a diverse and inclusive team leads
                                to better decision-making, greater creativity, and ultimately, better results for our
                                clients.
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-2 d-none d-lg-flex">
                    </div>
                    <div class="col-lg-2 d-none d-lg-flex">
                        <img class="" src="{{asset('img/quotes.svg')}}" alt="">
                    </div>
                </div>
            </div>
        </section>
        <!-- section start-->
        <section class="section">
            <div class="container">
                <div class="row flex-column-reverse flex-lg-row">
                    <div class="col-lg-6 mt-3">
                        <h3 class="heading__title pb-2">Contact Us</h3>
                        <p>
                            At SmartChain, our team is our greatest strength. We are a group of supply chain experts
                            with over 200 years of combined experience in leading supply chain solutions. Our team is
                            made up of domain experts, solution architects, technical architects, and consultants who
                            have successfully implemented leading supply chain solutions for customers across a wide
                            range of industries and segments, from small businesses to Fortune 500 companies.
                        </p>

                        <p class="text-danger">
                            Contact us today to learn more about how the SmartChain team can help your business optimize
                            its supply chain operations.
                        </p>
                    </div>
                    <div class="col-lg-6 mt-3">
                        <form class="form" action="javascript:void(0);" method="post" autocomplete="off">
                            <div class="row">
                                <div class="col-12">
                                    <h5 class="contact-form__subtitle">Send a message</h5>
                                </div>
                                <div class="col-md-6">
                                    <input class="form__field" type="text" name="name" placeholder="Your Name">
                                </div>
                                <div class="col-md-6">
                                    <input class="form__field" type="email" name="email" placeholder="Your Email">
                                </div>
                                <div class="col-md-6">
                                    <input class="form__field" type="tel" name="phone" placeholder="Your Phone">
                                </div>
                                <div class="col-md-6">
                                    <input class="form__field" type="text" name="subject" placeholder="Subject">
                                </div>
                                <div class="col-12">
                                    <textarea class="form__field form__message message--large" name="message"
                                              placeholder="Text"></textarea>
                                </div>
                                <div class="col-12">
                                    <a class="button button--filled" type="submit"><span>Send message</span>
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- section end-->
    </main>
@endsection
