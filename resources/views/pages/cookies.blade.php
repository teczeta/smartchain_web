@extends('layouts.app')

@section('content')
    <main class="main">

        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">Cookies Policy</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row bottom-30">
                    <div class="col-xl-10 offset-xl-1">
                        <h3 class="bottom-0">Cookies Policy</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-10 offset-xl-1">
                        <div class="container">
                            <p>SmartChain uses cookies to enhance the user experience and to track usage on our website.
                                Cookies
                                are small text files stored on a user&#39;s device that allow us to recognize repeat
                                visits and preferences.</p>
                            <p>By using our website, you consent to the use of cookies in accordance with this policy.
                                If you prefer,
                                you can disable cookies in your browser settings, but please note that this may affect
                                your ability to
                                use certain features of our website.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection
