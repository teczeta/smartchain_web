@extends('layouts.app')

@section('content')
    <main class="main">

        <!-- section start-->
        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">How we do it</h1>
                                <h5 class="text-white mt-3">
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="section history bg--lgray">
            <div class="container">
                <div class="row align-items-end">
                    <div class="col-md-12">
                        <div class="heading"><span class="heading__pre-title">How we do it</span>
                            <h3 class="heading__title">Our template based <span class="color--green"> Implementation approach</span></h3>
                            <p class="mt-4"> Our project implementation follows a template-based approach that consists
                                of the following steps</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-4">
                        <h4 class="bottom-20">Standardization
                        </h4>
                        <p>
                            This step involves standardizing the business processes through workshops
                            with stakeholders, optimizing processes, aligning with product features and
                            functionality,
                            conducting fit gap analysis, and defining the desired solution.
                        </p>
                    </div>
                    <div class="col-lg-7 col-xl-7 offset-xl-1 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits:</h6>
                        <ul class="list list--check list--reset">
                            <li class="list__item">Standardization workshops with business stakeholders</li>
                            <li class="list__item">Business Process Assessment and Optimization</li>
                            <li class="list__item">Process Alignment with Product Features and
                                functionality
                            </li>
                            <li class="list__item">Fit Gap Analysis</li>
                            <li class="list__item">Define To-Be Solution</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="section bg--lgray">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-4">
                        <h4 class="bottom-20">People
                        </h4>
                        <p>
                            The focus of this step is on identifying key functional business resources,
                            defining
                            project team roles and responsibilities, defining business process owners, and
                            establishing a
                            governance model.
                        </p>
                    </div>
                    <div class="col-lg-7 col-xl-7 offset-xl-1 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits:</h6>
                        <ul class="list list--check list--reset">
                            <li class="list__item">Identify Key
                                Functional
                                Business
                                Resources
                            </li>
                            <li class="list__item">Align Project
                                Team Roles and
                                Responsibilities
                            </li>
                            <li class="list__item">Define Business
                                Process Owners
                            </li>
                            <li class="list__item">Define Business
                                Data Quality
                                Owners
                            </li>
                            <li class="list__item">Implement
                                Governance
                                Model
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-4">
                        <h4 class="bottom-20">Product
                        </h4>
                        <p>
                            This step involves developing a high-level design, selecting the appropriate
                            product
                            modules, configuring the product, customizing code and deploying it,
                            standardizing master
                            data, and migrating data.
                        </p>
                    </div>
                    <div class="col-lg-7 col-xl-7 offset-xl-1 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits:</h6>
                        <ul class="list list--check list--reset">
                            <li class="list__item">High Level
                                Design and
                                package
                                module
                                selection
                            </li>
                            <li class="list__item">Product
                                configurations
                                and parameter
                                settings
                            </li>
                            <li class="list__item">Code
                                customizations
                                and deployment
                            </li>
                            <li class="list__item">Master Data
                                normalization
                                and
                                standardization
                            </li>
                            <li class="list__item">Master Data and
                                Transactional
                                Data migration
                                scripts
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="section bg--lgray">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-4">
                        <h4 class="bottom-20">Testing and Automation
                        </h4>
                        <p>
                            The focus of this step is on identifying test scenarios, creating
                            test cases that support business processes, standardizing test data and cases,
                            automating
                            test cases, and building a template for test results.
                        </p>
                    </div>
                    <div class="col-lg-7 col-xl-7 offset-xl-1 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits:</h6>
                        <ul class="list list--check list--reset">
                            <li class="list__item">Identify test
                                scenarios, build
                                test cases to
                                support
                                business
                                process flow
                            </li>
                            <li class="list__item">Prepare test
                                data and
                                standardize test
                                cases
                            </li>
                            <li class="list__item">Align test cases
                                implementation
                                template and
                                data
                            </li>
                            <li class="list__item">Automate test
                                cases
                            </li>
                            <li class="list__item">Build template
                                for test results
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row align-items-top">
                    <div class="col-lg-4">
                        <h4 class="bottom-20">Continued Support
                        </h4>
                        <p>
                            SmartChain provides comprehensive support services to ensure the smooth deployment, operation,
                            and maintenance of its technology solutions. Our team of experts offers technical support,
                            compliance support, root cause analysis, user training, and ongoing maintenance and upgrades. Our
                            support services help resolve any issues, ensure the technology is functioning optimally, and keep
                            customers up-to-date with regular software updates and upgrades.
                        </p>
                    </div>
                    <div class="col-lg-7 col-xl-7 offset-xl-1 top-50 top-lg-0">
                        <h6 class="bottom-20">Features and Benefits:</h6>
                        <ul class="list list--check list--reset">
                            <li class="list__item">Technical support for installation, configuration, and maintenance</li>
                            <li class="list__item">Compliance support to ensure industry standards and regulations are met</li>
                            <li class="list__item">Root cause analysis to solve underlying issues</li>
                            <li class="list__item">User training and documentation resources</li>
                            <li class="list__item">Ongoing maintenance and upgrades for continued optimal function.</li>

                        </ul>
                    </div>
                </div>
            </div>
        </section>


        <div class="cta-block cta-block--style-2"><img class="img--bg" src="{{asset('img/site/cta.webp')}}" alt="bg"/>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8">
                        <div class="heading heading--white">
                            <h3 class="heading__title title-small">
                                Contact us today to learn more about how the SmartChain team can help your business
                                optimize
                                its supply chain operations.
                            </h3>
                        </div>
                    </div>
                    <div class="col-lg-4 text-lg-right"><a class="button button--white"
                                                           href="{{route('contact-us')}}"><span>Get in touch</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @include('includes.insights')


    </main>
@endsection
