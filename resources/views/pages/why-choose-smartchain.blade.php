@extends('layouts.app')

@section('content')
    <main class="main">

        <!-- section start-->
        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">Why Choose SmartChain</h1>
                                <h5 class="text-white mt-3">

                                </h5>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row flex-column-reverse flex-lg-row align-items-center">
                    <div class="col-lg-7 top-50 top-lg-0">
                        <div class="heading bottom-20">
                            <h3 class="heading__title">Our Experience and Expertise</h3>
                        </div>
                        <ul class="list list--check list--reset">
                            <li class="list__item">Deep experience across industries and geographies.</li>
                            <li class="list__item">Experienced in all versions of the JDA (BY) WMS and TMS products.
                            </li>
                            <li class="list__item">A team of 50+ resources, built from careful selection and retention
                                of the best
                                resources in the market.
                            </li>
                            <li class="list__item">Experts who worked as SME/Customer Advisor for Tier 1 customers.</li>
                        </ul>
                    </div>
                    <div class="col-lg-5 px-5 order-md-last">
                        <img class="w-100" src="{{asset('img/site/exp.webp')}}" alt="img">
                    </div>
                </div>
            </div>
        </section>

        <section class="section bg--lgray">
            <div class="container">
                <div class="row flex-column-reverse flex-lg-row align-items-center">
                    <div class="col-lg-7 top-50 top-lg-0">
                        <div class="heading bottom-20">
                            <h3 class="heading__title">Strong on Solutions and Integration</h3>
                        </div>
                        <ul class="list list--check list--reset">
                            <li class="list__item">Ability to set TMS and WMS on multiple deployment models.</li>
                            <li class="list__item">Support and development of On-premise &amp; BY cloud and integration
                                with many
                                ERPs and middleware systems.
                            </li>
                            <li class="list__item">Ability to advise on best practices and improve processes without
                                harming budgets.
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-5 px-5 order-md-first">
                        <img class="w-100" src="{{asset('img/site/strong.webp')}}" alt="img">
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row flex-column-reverse flex-lg-row align-items-center">
                    <div class="col-lg-7 top-50 top-lg-0">
                        <div class="heading bottom-20">
                            <h3 class="heading__title">Our Customer Focus and Flexibility</h3>
                        </div>
                        <ul class="list list--check list--reset">
                            <li class="list__item">Helping customers to maximize Blue Yonder WMS/TMS investments and
                                minimize
                                costs.
                            </li>
                            <li class="list__item">Competitive offerings and attractive pricing models.</li>
                            <li class="list__item">Flexible with Customer Experience Processes and tools.</li>
                            <li class="list__item">Ability to use Agile or Waterfall project management approaches
                                depending on
                                circumstances and customer preferences.
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-5 px-5 order-md-last">
                        <img class="w-100" src="{{asset('img/site/flex.webp')}}" alt="img">
                    </div>
                </div>
            </div>
        </section>

        <section class="section bg--lgray">
            <div class="container">
                <div class="row flex-column-reverse flex-lg-row align-items-center">
                    <div class="col-lg-7 top-50 top-lg-0">
                        <div class="heading bottom-20">
                            <h3 class="heading__title">Driven to deliver Business Benefits and Results</h3>
                        </div>
                        <ul class="list list--check list--reset">
                            <li class="list__item">Ability to decrease freight cost spend and increase warehouse
                                efficiency.
                            </li>
                            <li class="list__item">Benchmarking of existing processes.</li>
                            <li class="list__item">Independent advice on best practices.</li>
                        </ul>
                    </div>
                    <div class="col-lg-5 px-5 order-md-first">
                        <img class="w-100" src="{{asset('img/site/benefits.webp')}}" alt="img">
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row flex-column-reverse flex-lg-row align-items-center">
                    <div class="col-lg-7 top-50 top-lg-0">
                        <div class="heading bottom-20">
                            <h3 class="heading__title">Our commitment to Sustainability</h3>
                        </div>
                        <p>
                            At SmartChain, we are committed to promoting sustainability in all aspects of our operations
                            and
                            services. With the help of our sustainability partner, PlanetWise, we ensure that we are
                            fully
                            compliant with all sustainability regulations and guidelines. Our approach to sustainability
                            is not just
                            about meeting regulatory requirements, but about incorporating it into our value system.
                        </p>
                        <ul class="list list--check list--reset">
                            <li class="list__item">We reduce our carbon footprint and promote energy efficiency in our
                                operations
                            </li>
                            <li class="list__item">Provide clients with a more sustainable supply chain solution</li>
                            <li class="list__item">Offer top-notch logistics technology services and solutions while
                                also contributing to a more
                                sustainable future
                            </li>
                            <li class="list__item">Confidence that working with SmartChain aligns with clients&#39;
                                sustainability goals and values.
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-5 px-5 order-md-last">
                        <img class="w-100" src="{{asset('img/site/pw.webp')}}" alt="img">
                    </div>
                </div>
            </div>
        </section>


        <div class="cta-block cta-block--style-2"><img class="img--bg" src="{{asset('img/site/cta.webp')}}" alt="bg"/>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8">
                        <div class="heading heading--white">
                            <h3 class="heading__title title-small">
                                Contact us today to learn more about how the SmartChain team can help your business
                                optimize
                                its supply chain operations.
                            </h3>
                        </div>
                    </div>
                    <div class="col-lg-4 text-lg-right"><a class="button button--white"
                                                           href="{{route('contact-us')}}"><span>Get in touch</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @include('includes.insights')


    </main>
@endsection
