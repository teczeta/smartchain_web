@extends('layouts.app')

@section('content')
    <main class="main">

        <!-- section start-->
        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">Supply Chain Execution</h1>
                                <h5 class="text-white mt-3">
                                    Streamline Your Supply Chain Execution with SmartChain's Solutions
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <!-- section end-->
        <section class="section service-details">
            <div class="container-fluid mx-5">
                <div class="container heading bottom-20"><span class="heading__pre-title">Overview</span>
                    <h3 class="heading__title">Our <span class="color--green">Supply Chain Execution </span>Solutions
                    </h3>
                    <h5 class="mt-4">&quot;Streamline Your Supply Chain Execution with SmartChain&#39;s
                        Solutions&quot;</h5>
                </div>
                <div class="row my-5">
                    <div class="col-xl-10 offset-xl-1">
                        <div class="tabs vertical-tabs r-tabs">
                            <ul class="vertical-tabs__header r-tabs-nav">
                                <li class="r-tabs-state-default r-tabs-tab"><a href="#vertical-tabs__item-1"
                                                                               class="r-tabs-anchor"><span>Transportation Planning and Allocation</span></a>
                                </li>
                                <li class="r-tabs-state-default r-tabs-tab"><a href="#vertical-tabs__item-2"
                                                                               class="r-tabs-anchor"><span>Transportation Optimisation</span></a>
                                </li>
                                <li class="r-tabs-state-default r-tabs-tab"><a href="#vertical-tabs__item-3"
                                                                               class="r-tabs-anchor"><span>Freight Audit &amp; Payment</span></a>
                                </li>
                                <li class="r-tabs-state-default r-tabs-tab"><a href="#vertical-tabs__item-4"
                                                                               class="r-tabs-anchor"><span>Warehouse Automation</span></a>
                                </li>
                                {{--<li class="r-tabs-state-default r-tabs-tab"><a href="#vertical-tabs__item-5"
                                                                               class="r-tabs-anchor"><span>Labour Management</span></a>--}}
                                </li>
                                <li class="r-tabs-state-default r-tabs-tab"><a href="#vertical-tabs__item-6"
                                                                               class="r-tabs-anchor"><span>Transport Modelling</span></a>
                                </li>
                                <li class="r-tabs-state-default r-tabs-tab"><a href="#vertical-tabs__item-7"
                                                                               class="r-tabs-anchor"><span>Fleet Management</span></a>
                                </li>
                                <li class="r-tabs-state-default r-tabs-tab"><a href="#vertical-tabs__item-8"
                                                                               class="r-tabs-anchor"><span>Carrier Onboarding</span></a>
                                </li>
                                <li class="r-tabs-state-default r-tabs-tab"><a href="#vertical-tabs__item-9"
                                                                               class="r-tabs-anchor"><span>Analytical Reporting</span></a>
                                </li>


                            </ul>
                            <div class="vertical-tabs__content">
                                <div class="vertical-tabs__item r-tabs-state-default r-tabs-panel"
                                     id="vertical-tabs__item-1" style="display: block;">
                                    <h5 class="tabs__title">Transportation Planning and Allocation</h5>

                                    <p>
                                        &quot;Streamline Your Logistics with SmartChain&#39;s Transportation
                                        Planning and Allocation&quot; Our transportation planning and allocation
                                        solution offers a comprehensive
                                        approach to optimizing your logistics operations. Our team of experts will work
                                        closely with you to
                                        fine-tune your transportation planning and allocation, resulting in improved
                                        efficiency and cost
                                        savings.
                                    </p>
                                    <div class="mt-5">
                                        <strong class="warehouse__subtitle">Features and benefits</strong>
                                        <ul class="list list--check list--reset">
                                            <li class="list__item">Enhance transportation efficiency</li>
                                            <li class="list__item">Cut down on transportation expenses</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="vertical-tabs__item r-tabs-state-default r-tabs-panel"
                                     id="vertical-tabs__item-2">
                                    <h5 class="tabs__title">Transportation Optimization</h5>
                                    <p>&quot;Elevate Your Supply Chain with SmartChain&#39;s Transportation
                                        Optimization&quot; Our transportation optimization solution is designed to
                                        streamline and optimize your
                                        logistics operations. Our team of experts will collaborate with you to refine
                                        your transportation
                                        optimization strategy, resulting in increased efficiency and
                                        cost-effectiveness.</p>
                                    <div class="mt-5">
                                        <strong class="warehouse__subtitle">Features and benefits</strong>
                                        <ul class="list list--check list--reset">
                                            <li class="list__item">Improve transportation efficiency</li>
                                            <li class="list__item">Decrease transportation costs</li>
                                            <li class="list__item">Increase end-to-end visibility</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="vertical-tabs__item r-tabs-state-default r-tabs-panel"
                                     id="vertical-tabs__item-3">
                                    <h5 class="tabs__title">Freight Audit &amp; Payment</h5>
                                    <p>&quot;Optimize Your Freight Audit and Payment with SmartChain&quot; Our freight
                                        audit and payment solution offers a comprehensive approach to streamline and
                                        optimize your
                                        freight audit and payment processes. Our team of experts will work with you to
                                        improve your freight
                                        audit and payment processes, resulting in cost savings.</p>
                                    <div class="mt-5">
                                        <strong class="warehouse__subtitle">Features and benefits</strong>
                                        <ul class="list list--check list--reset">
                                            <li class="list__item">Increase freight audit accuracy</li>
                                            <li class="list__item">Cut down on freight costs</li>
                                            <li class="list__item">Facilitate cross-functional collaboration</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="vertical-tabs__item r-tabs-state-default r-tabs-panel"
                                     id="vertical-tabs__item-4">
                                    <h5 class="tabs__title">Warehouse Automation</h5>
                                    <p>
                                        &quot;Boost Your Warehouse Operations with SmartChain&#39;s Automation
                                        Solutions&quot; Our warehouse automation solutions provide a comprehensive
                                        approach to enhancing
                                        your warehouse operations. Whether you&#39;re looking to improve your warehouse
                                        automation, labor
                                        management, or inventory management, our team of experts has the experience and
                                        knowledge to
                                        help you achieve your business goals.
                                    </p>
                                    <div class="mt-5">
                                        <strong class="warehouse__subtitle">Features and benefits</strong>
                                        <ul class="list list--check list--reset">
                                            <li class="list__item">Increase warehouse productivity</li>
                                            <li class="list__item">Enhance inventory turnover</li>
                                            <li class="list__item">Reduce lead times</li>
                                            <li class="list__item">Improve capacity utilization</li>
                                            <li class="list__item">Boost production efficiency</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="vertical-tabs__item r-tabs-state-default r-tabs-panel"
                                     id="vertical-tabs__item-6">
                                    <h5 class="tabs__title">Transport Modelling</h5>
                                    <p>
                                        Our transport modelling solutions offer a comprehensive approach to optimizing
                                        your logistics
                                        network. Our team of experts will work closely with you to refine your transport
                                        modelling strategy,
                                        resulting in improved efficiency and cost savings through network optimization
                                        and effective route
                                        planning.
                                    </p>
                                    <div class="mt-5">
                                        <strong class="warehouse__subtitle">Features and benefits</strong>
                                        <ul class="list list--check list--reset">
                                            <li class="list__item">Optimize your logistics network</li>
                                            <li class="list__item">Improve route planning</li>
                                            <li class="list__item">Enhance supply chain visibility</li>
                                            <li class="list__item">Reduce transportation costs</li>
                                            <li class="list__item">Increase operational efficiency</li>
                                            <li class="list__item">Strengthen risk management</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="vertical-tabs__item r-tabs-state-default r-tabs-panel"
                                     id="vertical-tabs__item-7">
                                    <h5 class="tabs__title">Fleet Management</h5>
                                    <p>
                                        Our fleet management solutions offer a comprehensive approach to optimizing your
                                        fleet
                                        operations. Whether you are looking to improve your asset utilization, reduce
                                        costs, or increase
                                        efficiency, our team of experts has the experience and knowledge to help you
                                        achieve your goals.
                                        With our fleet management solutions, you can make informed decisions, improve
                                        communication
                                        and collaboration, and gain real-time insights into your fleet operations.
                                    </p>
                                    <div class="mt-5">
                                        <strong class="warehouse__subtitle">Features and benefits</strong>
                                        <ul class="list list--check list--reset">
                                            <li class="list__item">Enhance asset utilization</li>
                                            <li class="list__item">Reduce fleet costs</li>
                                            <li class="list__item">Improve efficiency and productivity</li>
                                            <li class="list__item">Gain real-time insights into fleet operations</li>
                                            <li class="list__item">Enhance communication and collaboration</li>
                                            <li class="list__item">Make informed decisions</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="vertical-tabs__item r-tabs-state-default r-tabs-panel"
                                     id="vertical-tabs__item-8">
                                    <h5 class="tabs__title">Carrier Onboarding</h5>
                                    <p>
                                        Our carrier onboarding solution offers a comprehensive approach to streamlining
                                        and optimizing
                                        your carrier onboarding processes. Our team of experts will work closely with
                                        you to fine-tune your
                                        carrier onboarding, ensuring that your carriers are onboarded efficiently and
                                        effectively.
                                    </p>
                                    <div class="mt-5">
                                        <strong class="warehouse__subtitle">Features and benefits</strong>
                                        <ul class="list list--check list--reset">
                                            <li class="list__item">Streamline EDI processes</li>
                                            <li class="list__item">Automate testing and training</li>
                                            <li class="list__item">Enhance end-to-end visibility</li>
                                            <li class="list__item">Improve cross-functional collaboration</li>
                                            <li class="list__item">Optimize carrier onboarding efficiency</li>
                                            <li class="list__item">Reduce onboarding costs</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="vertical-tabs__item r-tabs-state-default r-tabs-panel"
                                     id="vertical-tabs__item-9">
                                    <h5 class="tabs__title">Analytical Reporting</h5>
                                    <strong>&quot;Unlock the Power of Data with SmartChain&#39;s Analytical Reporting Solutions&quot;</strong>
                                    <p>
                                        Our analytical reporting solutions provide cutting-edge technology and data analysis to help
                                        companies make data-driven decisions. Our AI-based solutions deliver real-time insights, tracking,
                                        and reporting that enable businesses to optimize their supply chain operations. With advanced
                                        machine learning algorithms, our solutions provide a comprehensive view of supply chain
                                        performance and help companies identify areas for improvement.
                                    </p>
                                    <div class="mt-5">
                                        <strong class="warehouse__subtitle">Features and benefits</strong>
                                        <ul class="list list--check list--reset">
                                            <li class="list__item">Unleash the power of data with AI-based solutions</li>
                                            <li class="list__item">Get real-time insights and tracking</li>
                                            <li class="list__item">Maximize supply chain performance with comprehensive reporting</li>
                                            <li class="list__item">Identify areas for improvement with machine learning algorithms</li>
                                            <li class="list__item">Make data-driven decisions to optimize operations</li>
                                            <li class="list__item">Stay ahead of the competition with cutting-edge technology</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section bg--dgray"><img class="section--bg t50 r0" src="{{asset('img/testimonials-bg.png')}}"
                                                alt="img">
            <div class="container">
                <div class="row bottom-70">
                    <div class="col-12">
                        <div class="heading heading--white"><span class="heading__pre-title">Steps for work</span>
                            <h3 class="heading__title">How our Supply chain execution solutions can impact your
                                Business</h3>
                        </div>
                    </div>
                </div>
                <div class="row offset-50">
                    <div class="col-md-6 col-xl-3">
                        <div class="icon-item icon-item--white">
                            <div class="icon-item__count"><span>01</span></div>
                            <h5 class="icon-item__title">Improve transportation efficiency</h5>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-3">
                        <div class="icon-item icon-item--white">
                            <div class="icon-item__count"><span>02</span></div>
                            <h5 class="icon-item__title">Reduce transportation costs</h5>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-3">
                        <div class="icon-item icon-item--white">
                            <div class="icon-item__count"><span>03</span></div>
                            <h5 class="icon-item__title"> Improve warehouse productivity</h5>
                        </div>
                    </div>
                    <div class="col-md-6 col-xl-3">
                        <div class="icon-item icon-item--white">
                            <div class="icon-item__count"><span>04</span></div>
                            <h5 class="icon-item__title"> Increase end-to-end visibility</h5>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <div class="cta-block cta-block--style-2"><img class="img--bg" src="{{asset('img/site/cta.webp')}}" alt="bg"/>
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-8">
                        <div class="heading heading--white">
                            <h3 class="heading__title title-small">
                                Contact us today to learn more about how the SmartChain team can help your business
                                optimize
                                its supply chain operations.
                            </h3>
                        </div>
                    </div>
                    <div class="col-lg-4 text-lg-right"><a class="button button--white"
                                                           href="{{route('contact-us')}}"><span>Get in touch</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        @include('includes.insights')


    </main>
@endsection
