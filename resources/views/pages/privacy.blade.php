@extends('layouts.app')

@section('content')
    <main class="main">

        <section class="hero-block">
            <picture>
                <source srcset="{{asset('img/site/hero.webp')}}" media="(min-width: 992px)"/>
                <img class="img--bg" src="{{asset('img/site/hero.webp')}}" alt="img"/>
            </picture>
            <div class="hero-block__layout"></div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="align-container">
                            <div class="align-container__item"><span class="hero-block__overlay">SmartChain</span>
                                <h1 class="hero-block__title">Privacy Policy</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="row bottom-30">
                    <div class="col-xl-10 offset-xl-1">
                        <h3 class="bottom-0">Privacy Policy</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-10 offset-xl-1">
                        <div class="container">
                           <p>At SmartChain, we understand the importance of privacy and are committed to protecting the
                               personal information of our users. Our Privacy Policy outlines the types of information we collect,
                               how we use it, and the steps we take to protect it.</p>
                           <p>We collect information that is necessary to provide our services, such as name, contact information,
                               and payment information. This information is used to complete transactions, provide customer
                               support, and improve our services.</p>
                           <p>SmartChain takes appropriate measures to secure the information we collect, including
                               implementing secure servers, firewalls, and encryption technology. We also regularly review and
                               update our security measures to ensure the protection of our users&#39; information.</p>
                           <p>We do not sell or share our users&#39; personal information with third parties, except as required by law
                               or with the user&#39;s consent.</p>
                           <p>If you have any questions about our Privacy Policy, please contact us at inquiry@smartchain.co</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>
@endsection
